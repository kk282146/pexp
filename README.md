# Environment: #
MySQL Community Server 5.6.21

Java 8 Update 25 JDK

[Tomcat 8.0.15](http://tomcat.apache.org/download-80.cgi) (On Windows use Service Installer version).

IDEA Ultimate Edition 14.0.1

#Running the project #
Start IDEA and choose "import from VCS" to clone the project from the repository.

Recompile the project. Maybe, it will ask you to choose JDK version.
If it isn't works and IDEA says that there is no Maven - there is the installation guide: [Maven 3.2.3](http://maven.apache.org/download.cgi).

Enter Run-EditConfiguration and add (using "+" button) configuration for Tomcat. In that configuration enter the name of config (eg. "Tomcat 8"), choose the Tomcat wersion and add to the list "Before launch" (at the bottom) item "Build Artifact" with artifact "PEXP:war exploded".

Press green "Play" triangle on the right top of the page and check in localhost:8080 shows the webpage.

#Configuration of AngularJS. Integration of ng-boilerplate for AngularJS development#

0. You need to run your console as administrator. 

1. Install node.js from http://nodejs.org/

2. Go to the folder /src/main/webapp/creator.

3. Install bower: "npm install -g bower".

4. Install grunt: "npm install -g grunt-cli".

5. Run command "npm install && bower install && grunt"


#Configuration of PostgreSQL database and migrations#

1) Download and install [PostgreSQL](http://www.enterprisedb.com/products-services-training/pgdownload)

2) Create user 'postgres' with superuser rights and password 'zpp2015pexp'. 
'postgres' is the default superuser name, so it is enough just to enter that password during the installation of the database.

3) Update maven dependencies in Idea.

4) In Maven tab (usually on right panel) run compile and then in plugins open flyway plugin and start 'migrate' task.
Last step have to run migration, i.e. create all the necessary tables in the database.