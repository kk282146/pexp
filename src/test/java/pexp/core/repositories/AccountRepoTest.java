package pexp.core.repositories;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import pexp.core.models.entities.Account;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring/business-config.xml")
public class AccountRepoTest {

    @Autowired
    private AccountRepo repo;

    private Account account;

    @Before
    @Transactional
    @Rollback(false)
    public void setup()
    {
        account = new Account();
        account.setFirstName("Gandalf");
        account.setLastName("White");
        account.setEmail("Gandalf73@palantir.com");
        account.setPassword("password");
        repo.save(account);
    }

    @Test
    @Transactional
    public void testFind()
    {
        Account account = repo.findOne(this.account.getId());
        assertNotNull(account);
        assertEquals(account.getEmail(), "Gandalf73@palantir.com");
        assertEquals(account.getPassword(), "password");
    }
}
