package pexp.mvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import pexp.core.models.entities.Account;
import pexp.core.models.entities.Experiment;
import pexp.core.repositories.AccountRepo;
import pexp.core.repositories.ExperimentRepo;
import pexp.rest.controllers.ExperimentController;
import pexp.rest.resources.ExperimentResource;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/business-config.xml", "classpath:mvc-dispatcher-servlet.xml"})
@WebAppConfiguration
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        /*DbUnitTestExecutionListener.class*/ })
public class ExperimentIntegrationTests {
    @Autowired
    protected ExperimentController controller;

    protected MockMvc mockMvc;

    protected Account account;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        account = new Account();
        account.setId(1L);
        account.setFirstName("Gandalf");
        account.setLastName("White");
        account.setEmail("Gandalf73@palantir.com");
        account.setPassword("password");

        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");

        mockMvc = MockMvcBuilders.standaloneSetup(controller).setViewResolvers(viewResolver).build();

        experiment = new ExperimentResource();
        experiment.setTitle("Test Title");
        experiment.setContent("[{\"title\":\"plec\",\"text\":\"plec\",\"id\":1,\"$$hashKey\":\"003\"},{\"title\":\"wiek\",\"text\":\"jaki wiek?\",\"id\":2,\"$$hashKey\":\"005\"}]");
    }
    @Autowired
    protected AccountRepo accountRepo;

    @Autowired
    protected ExperimentRepo expRepo;

    private ExperimentResource experiment;
    
    @After
    public void cleanup() {
        Account accountForDelete = null;
        if (account.getEmail() != null)
            accountForDelete = accountRepo.findByEmail(account.getEmail());
        
        if (accountForDelete != null) {
            Experiment created = expRepo.findByTitleAndAuthorId("Test Title", accountForDelete.getId());
            if (created != null) {
                expRepo.delete(created.getId());
            }
            accountRepo.delete(accountForDelete.getId());
        }
            
    }
    
    @Test
    public void createExperimentWithDatabase() throws Exception {      

        account.setId(null);
        account = accountRepo.save(account);

        mockMvc.perform(post("/experiments")
                        .content(new ObjectMapper().writeValueAsString(experiment))
                        .contentType(MediaType.APPLICATION_JSON)
                        .with(user("Gandalf73@palantir.com").roles("USER"))
        )
                    .andDo(print())
                    .andExpect(jsonPath("$.title", is(experiment.getTitle())))
                    .andExpect(status().isCreated());

        assertNotNull(experiment);
        Experiment created = expRepo.findByTitleAndAuthorId("Test Title", account.getId());
        assertNotNull(created);
        assertEquals(created.getTitle(), experiment.getTitle());
        assertEquals(created.getAuthor().getId(), account.getId());
        assertEquals(created.getContent().getJsonString(), experiment.getContent());
    }
}
