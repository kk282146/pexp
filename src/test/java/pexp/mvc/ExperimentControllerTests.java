package pexp.mvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import pexp.core.models.entities.Account;
import pexp.core.models.entities.Experiment;
import pexp.core.security.AccountUserDetails;
import pexp.core.services.AccountService;
import pexp.core.services.ExperimentService;
import pexp.core.services.exceptions.AccountDoesNotExistException;
import pexp.core.services.exceptions.ExperimentExistsException;
import pexp.core.services.exceptions.ExperimentNotFoundException;
import pexp.rest.controllers.ExperimentController;
import pexp.rest.resources.asm.ExperimentResourceAsm;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/business-config.xml", "classpath:mvc-dispatcher-servlet.xml"})
@WebAppConfiguration
public class ExperimentControllerTests {
    @InjectMocks
    protected ExperimentController controller;

    @Mock
    protected ExperimentService service;

    @Mock
    protected AccountService accountService;

    protected MockMvc mockMvc;

    protected Account account;

    protected AccountUserDetails userDetails;
    
    protected Experiment experiment;
    protected String EXP_KEY = "aaaaaa"; 

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        account = new Account();
        account.setId(1L);
        account.setFirstName("Gandalf");
        account.setLastName("White");
        account.setEmail("Gandalf73@palantir.com");
        account.setPassword("password");
        when(accountService.findAccount(eq(1L))).thenReturn(account);
        when(accountService.findAccount(eq(1L))).thenReturn(account);
        when(accountService.findByEmail(eq("Gandalf73@palantir.com"))).thenReturn(account);


        experiment = new Experiment();
        experiment.setId(1L);
        experiment.setTitle("Test Title");
        experiment.setContent("{}");
        experiment.setShareKey(EXP_KEY);

        userDetails = new AccountUserDetails(account);        
        
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");

        mockMvc = MockMvcBuilders.standaloneSetup(controller)
                .setViewResolvers(viewResolver)
                .build();
    }


    @Test
    public void getExistingExperiment() throws Exception {      

        when(service.findByShareKey(EXP_KEY)).thenReturn(experiment);

        mockMvc.perform(get("/experiments/" + EXP_KEY)).andDo(print())
                .andExpect(jsonPath("$.title", is(experiment.getTitle())))
                .andExpect(jsonPath("$.links[*].href", hasItem(endsWith("experiments/" + EXP_KEY))))
                .andExpect(status().isOk());
    }

    @Test
    public void getNonExistingExperiment() throws Exception {
       
        when(service.findByShareKey(EXP_KEY)).thenReturn(null);

        mockMvc.perform(get("experiments/" + EXP_KEY)).andDo(print())
                .andExpect(status().isNotFound());
    }



    @Test
    public void createExperimentExistingAccount() throws Exception {
        
        ObjectMapper mapper = new ObjectMapper();

        when(service.saveExperiment(eq(account.getId()), any(Experiment.class))).thenReturn(experiment);

        mockMvc.perform(post("/experiments")
                        .with(user(userDetails))
                        .content(mapper.writeValueAsString(new ExperimentResourceAsm().toResource(experiment)))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(jsonPath("$.title", is(experiment.getTitle())))
                .andExpect(jsonPath("$.links[*].href", hasItem(endsWith("experiments/" + EXP_KEY))))
                .andExpect(status().isCreated());
    }

    @Test
    public void createExperimentNonExistingAccount() throws Exception {
        when(service.saveExperiment(eq(1L), any(Experiment.class))).thenThrow(new AccountDoesNotExistException());

        mockMvc.perform(post("/experiments")
                .with(user(userDetails))
                .content("{\"title\":\"Test Title\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void createExperimentExistingExperimentName() throws Exception {
        when(service.saveExperiment(eq(1L), any(Experiment.class))).thenThrow(new ExperimentExistsException());

        mockMvc.perform(post("/experiments")
                .with(user(userDetails))
                .content("{\"title\":\"Test Title\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict());
    }

    @Test
    public void findAllExperimentsByAuthor() throws Exception {
        List<Experiment> list = new ArrayList<>();
        
        Account author = accountService.findAccount(1L);
        
        list.add(experiment);

        Experiment experimentB = new Experiment();
        experimentB.setId(2L);
        experimentB.setTitle("Title B");
        experimentB.setAuthor(author);
        experimentB.setShareKey("aaabbb");
        list.add(experimentB);

        when(service.findExperimentsByAuthor(1L)).thenReturn(list);

        mockMvc.perform(get("/experiments")
                .with(user(userDetails)))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void findAllExperimentsForNonExistingAuthor() throws Exception {
        when(service.findExperimentsByAuthor(1L)).thenThrow(new AccountDoesNotExistException());

        mockMvc.perform(get("/experiments/author/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void removeExperiment() throws Exception
    {
        Mockito.doNothing().when(service).deleteExperiment(eq(account.getId()), any(String.class));

        mockMvc.perform(post("/experiments/remove/idiifk")
            .with(user(userDetails))
        )
        .andExpect(status().isFound())
        .andExpect(redirectedUrl("/experiments"));
    }

    @Test
    public void removeNonExistingExperiment() throws Exception
    {
        Mockito.doThrow(new ExperimentNotFoundException()).when(service).deleteExperiment(eq(account.getId()), any(String.class));

        mockMvc.perform(post("/experiments/remove/idiifk")
                        .with(user(userDetails)))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/experiments"));
    }
}
