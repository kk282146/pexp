<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>

<head>

  <%@ page contentType="text/html; charset=UTF-8" %>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>PEXP - Platforma eksperymentów psychologicznych</title>

  <!-- font awesome from BootstrapCDN -->
  <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

  <!-- compiled CSS -->
  <link rel="stylesheet" type="text/css" href="/creator/assets/ngbp-0.3.2.css" />


  <!-- Custom CSS -->
  <link href="/css/landing-page.css" rel="stylesheet">
</head>

<body>

<c:set var="authentication" value="${pageContext.request.userPrincipal}"/>
<!-- Navigation -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">PEXP</a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li>
          <a href="/experiments">Moje testy</a>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <c:choose>
          <c:when test="${authentication != null}">
            <c:set var="currentUser" value="${authentication.name}"/>
            <li class="navbar-text">
              <c:out value="${currentUser}"/>
            </li>
            <li>
              <form:form class="navbar-form pull-right" action="/logout" method="POST">
                <input type="submit" class="btn" value="Wyloguj się" />
              </form:form>
            </li>
          </c:when>
          <c:otherwise>
            <li>
              <a href="/login">Zaloguj się</a>
            </li>
            <li>
              <a href="/signup">Załóż konto</a>
            </li>
          </c:otherwise>
        </c:choose>
      </ul>
    </div>
    <!-- /.navbar-collapse -->
  </div>
  <!-- /.container -->
</nav>

<!-- Page Content -->

<div class="content-section-a">

  <div class="container">

    <div class="row">
      <div class="col-lg-7 col-sm-11">
        <hr class="section-heading-spacer">
        <div class="clearfix"></div>
        <h2 class="section-heading">Twoje eksperymenty</h2>

        <c:if test="${empty experiments}">
            <p class="lead">Aktualnie nie masz żadnych eksperymentów.</p>
        </c:if>
        <c:if test="${not empty experiments}">
          <table class="table">
            <c:forEach var="exp" items="${experiments}">
              <tr>
                <td><a id="link_${exp.getExpId()}" href="/tester/tester.html?shareKey=${exp.getShareKey()}">
                  ${exp.getTitle()}
                </a></td>
                <td><button class="copy-button btn btn-info" id="but_${exp.getExpId()}"
                            title="Naciśnij, aby skopiować link do ">Skopiuj link</button></td>
                <td><a href="/creator/index.html?shareKey=${exp.getShareKey()}" class="btn btn-default">Edytuj</a></td>
                <td><a href="/experiments/remove/${exp.getShareKey()}" class="btn btn-danger">Usuń</a></td>
                <td><a href="/replies/${exp.getShareKey()}" class="btn btn-default">Wyniki</a></td>
                <td><a href="/replies/xls/${exp.getShareKey()}">${exp.getTitle()}.xls</a></td>
              </tr>
            </c:forEach>
          </table>
        </c:if>
        <ul class="list-inline intro-social-buttons">
          <li>
            <a href="/creator/index.html" class="btn btn-default btn-lg"><span class="network-name">Stwórz eksperyment</span></a>
          </li>
        </ul>
      </div>
    </div>

  </div>
  <!-- /.container -->

</div>
<!-- /.content-section-a -->


<!-- Footer -->
<footer>
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <ul class="list-inline">
        </ul>
        <p class="copyright text-muted small">Copyright &copy; Grupa ZPP 2014. All Rights Reserved</p>
      </div>
    </div>
  </div>
</footer>

<!-- compiled JavaScript -->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="/js/ZeroClipboard.min.js"></script>
<script type="text/javascript" src="/js/copyLink.js"></script>

</body>

</html>
