<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>PEXP - Logowanie</title>

  <!-- font awesome from BootstrapCDN -->
  <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

  <!-- compiled CSS -->
  <link rel="stylesheet" type="text/css" href="/creator/assets/ngbp-0.3.2.css" />

  <!-- compiled JavaScript -->
  <script type="text/javascript" src="/creator/vendor/angular/angular.js"></script>
  <script type="text/javascript" src="/creator/vendor/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
  <script type="text/javascript" src="/creator/vendor/placeholders/angular-placeholders-0.0.1-SNAPSHOT.min.js"></script>
  <script type="text/javascript" src="/creator/vendor/angular-ui-router/release/angular-ui-router.js"></script>
  <script type="text/javascript" src="/creator/templates-common.js"></script>
  <script type="text/javascript" src="/creator/templates-app.js"></script>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Custom CSS -->
  <link href="/css/landing-page.css" rel="stylesheet">
  <link href="/css/login.css" rel="stylesheet">
</head>

<body>

<c:set var="authentication" value="${pageContext.request.userPrincipal}"/>
<!-- Navigation -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
       <a class="navbar-brand" href="/">PEXP</a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li>
          <a href="/experiments">Moje testy</a>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <c:choose>
          <c:when test="${authentication != null}">
            <c:set var="currentUser" value="${authentication.name}"/>
            <li class="navbar-text">
              <c:out value="${currentUser}"/>
            </li>
            <li>
              <form:form class="navbar-form pull-right" action="/logout" method="POST">
                <input type="submit" class="btn" value="Log out" />
              </form:form>
            </li>
          </c:when>
          <c:otherwise>
            <li>
              <a href="/login">Zaloguj się</a>
            </li>
            <li>
              <a href="/signup">Załóż konto</a>
            </li>
          </c:otherwise>
        </c:choose>
      </ul>
    </div>
    <!-- /.navbar-collapse -->
  </div>
  <!-- /.container -->
</nav>

<!-- Page Content -->

<div class="content-section-a">

  <div class="container">

    <form class="form-signin" role="form" name='loginForm' th:action="@{/login}" method='POST'>
      <h2 class="form-signin-heading">Logowanie</h2>
      <% if (request.getParameter("error") != null) { %>
      <div class="alert alert-danger" role="alert">
        Niepoprawny adres e-mail lub hasło.
      </div>
      <% } %>

      <label for="username" class="sr-only">Adres e-mail</label>
      <input id="username" name="username" class="form-control" placeholder="Adres e-mail" required autofocus>
      <label for="password" class="sr-only">Hasło</label>
      <input type="password" id="password" name="password" class="form-control" placeholder="Hasło" required>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Zaloguj się</button>
    </form>

  </div>
  <!-- /.container -->

</div>
<!-- /.content-section-a -->


<!-- Footer -->
<footer>
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <ul class="list-inline">
        </ul>
        <p class="copyright text-muted small">Copyright &copy; Grupa ZPP 2014. All Rights Reserved</p>
      </div>
    </div>
  </div>
</footer>

</body>

</html>