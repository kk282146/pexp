<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>

<head>

    <%@ page contentType="text/html; charset=UTF-8" %>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PEXP - Platforma eksperymentów psychologicznych</title>

    <!-- font awesome from BootstrapCDN -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

    <!-- compiled CSS -->
    <link rel="stylesheet" type="text/css" href="/creator/assets/ngbp-0.3.2.css" />

    <!-- compiled JavaScript -->
    <script type="text/javascript" src="/creator/vendor/angular/angular.js"></script>
    <script type="text/javascript" src="/creator/vendor/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
    <script type="text/javascript" src="/creator/vendor/placeholders/angular-placeholders-0.0.1-SNAPSHOT.min.js"></script>
    <script type="text/javascript" src="/creator/vendor/angular-ui-router/release/angular-ui-router.js"></script>
    <script type="text/javascript" src="/creator/templates-common.js"></script>
    <script type="text/javascript" src="/creator/templates-app.js"></script>


    <!-- Custom CSS -->
    <link href="/css/landing-page.css" rel="stylesheet">
</head>

<body>
<div id="wrap">

    <c:set var="authentication" value="${pageContext.request.userPrincipal}"/>
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">PEXP</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="/experiments">Moje testy</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <c:choose>
                        <c:when test="${authentication != null}">
                            <c:set var="currentUser" value="${authentication.name}"/>
                            <li class="navbar-text">
                                <c:out value="${currentUser}"/>
                            </li>
                            <li>
                                <form:form class="navbar-form pull-right" action="/logout" method="POST">
                                    <input type="submit" class="btn" value="Wyloguj się" />
                                </form:form>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li>
                                <a href="/login">Zaloguj się</a>
                            </li>
                            <li>
                                <a href="/signup">Załóż konto</a>
                            </li>
                        </c:otherwise>
                    </c:choose>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Header -->
    <div class="center-container">
        <div class="logo"> </div>
        <div class="intro-message">
            <h3>Platforma eksperymentów psychologicznych</h3>
            <hr class="intro-divider">
            <ul class="list-inline intro-social-buttons">
                <li>
                    <spring:url value="/login" var="loginUrl"/>
                    <a href="${loginUrl}" class="btn btn-info btn-lg"><span class="network-name">Zaloguj się</span></a>
                </li>
                <li>
                    <spring:url value="/signup" var="signupUrl"/>
                    <a href="${signupUrl}" class="btn btn-info btn-lg"><span class="network-name">Załóż konto</span></a>
                </li>
            </ul>

        </div>
        <!-- /.container -->
    </div>
</div>
</body>

</html>
