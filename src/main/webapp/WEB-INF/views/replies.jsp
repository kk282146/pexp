<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html>

<head>
    <!-- Latest compiled and minified CSS -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link href="/css/table.css" rel="stylesheet">
    <title>PEXP - Wyniki eksperymentu</title>

</head>

<body>
    <div class="col-sm-6 col-sm-offset-3 col-md-8 col-md-offset-2 main">

      <c:if test="${!replies.isEmpty()}">
        <div class="table-responsive">
          <table id="keywords" cellspacing="0" cellpadding="0" class="table table-striped">
            <thead>
            <tr>
              <c:forEach var="name" items="${names}">
                <th colspan="${widths.get(name)}">${name}</th>
              </c:forEach>
            </tr>
            <tr>
                <c:forEach var="name" items="${names}">
                    <th colspan="${widths.get(name)}">${type.get(name)}</th>
                </c:forEach>
            </tr>
            </thead>
            <tbody>
            <tr>
                <c:forEach var="name" items="${names}">
                  <c:forEach var="label" items="${labels.get(name)}">
                      <td>${label.toString()}</td>
                  </c:forEach>
                </c:forEach>
            </tr>
              <c:forEach var="rep" items="${replies}">
                <tr>
                    <c:forEach var="name" items="${names}">
                        <c:forEach var="label" items="${labels.get(name)}">
                            <td>${rep.get(name.toString()).get(label.toString()).toString()}</td>
                        </c:forEach>
                    </c:forEach>
                </tr>
              </c:forEach>
            </tbody>
          </table>
        </div>
      </c:if>
    </div>

</body>
</html>
