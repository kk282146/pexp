describe('CreatorController', function() {
    beforeEach(module('creatorApp'));

    var $scope, qList;


    beforeEach(inject(function($controller, $rootScope, questionsList){

        $scope = $rootScope.$new();
        qList = questionsList;
        $controller('CreatorController', {$scope: $scope});

    }));

    describe('submit 2 question', function() {
        it('checks if questions submitting properly', function () {
            //arrange
            var q1 = qList.initNewQuestion();
            q1.ans_type = "time";
            q1.title = "question 1";
            q1.text = "question 1 text";

            var q2 = qList.initNewQuestion();
            q2.ans_type = "text";
            q2.title = "question 2";
            q2.text = "question 2 text";
            q2.answers = {0: 'ans1'};

            //act
            $scope.newQuestion = q1;
            $scope.submitQuestion();

            $scope.newQuestion = q2;
            $scope.submitQuestion();

            //assert
            changed1 = qList.getQuestions()[0];
            expect(changed1.title).toEqual('question 1');
            expect(changed1.ans_type).toEqual('time');
            expect(changed1.id).toEqual(1);
            expect(changed1.answers).toEqual({});

            changed2 = qList.getQuestions()[1];
            expect(changed2.title).toEqual('question 2');
            expect(changed2.ans_type).toEqual('text');
            expect(changed2.id).toEqual(2);
            expect(changed2.answers).toEqual({0: 'ans1'});
        });
    });

    describe('fail to submit empty question', function() {
        it('checks if answers are changed when duration is selected', function() {
            //arrange
            $scope.newQuestion = qList.initNewQuestion();
            $scope.newQuestion.ans_type = "time";

            //act
            $scope.submitQuestion();

            //assert
            changed = qList.getQuestions()[0];
            expect(changed).toBe(undefined);
        });
    });

    describe('question with time answer test', function() {
        it('checks if answers are changed when duration is selected', function() {
            //arrange
            $scope.checked_duration = true;
            $scope.newQuestion = qList.initNewQuestion();
            $scope.newQuestion.ans_type = "time";
            $scope.newQuestion.text = "question text";
            $scope.newQuestion.title = "some_title_1";

            //act
            $scope.submitQuestion();

            //assert
            changed = qList.getQuestions()[0];
            expect(changed.answers).toEqual({ 'checked_duration': true });
        });

        it('checks if answers are changed when duration is not selected', function() {
            //arrange
            $scope.checked_duration = false;
            $scope.newQuestion = qList.initNewQuestion();
            $scope.newQuestion.ans_type = "time";
            $scope.newQuestion.text = "question text";
            $scope.newQuestion.title = "some_title_2";

            //act
            $scope.submitQuestion();

            //assert
            changed = qList.getQuestions()[0];
            expect(changed.answers).toEqual({'checked_duration': false });
        });
    });

    describe('question with date answer test', function() {
        it('checks if answers are changed when year is selected', function() {

            $scope.checked_year = true;
            $scope.checked_time = false;

            $scope.newQuestion = qList.initNewQuestion();
            $scope.newQuestion.ans_type = "date";
            $scope.newQuestion.text = "question text";
            $scope.newQuestion.title = "some_title_3";

            $scope.submitQuestion();

            expect(qList.getQuestions()[0].answers).toEqual({ "checked_year" : true, "checked_time": false });
        });

        it('checks if answers are changed when time and year are selected', function() {

            $scope.checked_time = true;
            $scope.checked_year = true;

            $scope.newQuestion = qList.initNewQuestion();
            $scope.newQuestion.ans_type = "date";
            $scope.newQuestion.text = "question text";
            $scope.newQuestion.title = "some_title_41";

            $scope.submitQuestion();

            expect(qList.getQuestions()[0].answers).toEqual({ "checked_year" : true, "checked_time": true});
        });

        it('checks if answers are changed when time is selected', function() {

            $scope.checked_year = false;
            $scope.checked_time = true;

            $scope.newQuestion = qList.initNewQuestion();
            $scope.newQuestion.ans_type = "date";
            $scope.newQuestion.text = "question text";
            $scope.newQuestion.title = "some_title_4";

            $scope.submitQuestion();

            expect(qList.getQuestions()[0].answers).toEqual({ "checked_year" : false, "checked_time": true});
        });

        it('checks if answers are changed when nothing is selected', function() {

            $scope.checked_time = false;
            $scope.checked_year = false;

            $scope.newQuestion = qList.initNewQuestion();
            $scope.newQuestion.ans_type = "date";
            $scope.newQuestion.text = "question text";
            $scope.newQuestion.title = "some_title_5";

            $scope.submitQuestion();

            expect(qList.getQuestions()[0].answers).toEqual({ "checked_year" : false, "checked_time": false});
        });
    });

    describe('grid question test', function() {
        it('checks addNewGridText function', function() {
            $scope.newQuestion = qList.initNewQuestion();
            $scope.newQuestion.ans_type = "grid";
            $scope.newQuestion.text = "question text";
            $scope.newQuestion.title = "some_title_6";

            $scope.row_text = "Row 1";
            $scope.addNewGridText(true);
            $scope.col_text = "Col 1";
            $scope.addNewGridText(false);

            expect($scope.newQuestion.ans_type).toEqual("grid");

            expect($scope.newQuestion.answers[0].text).toEqual("Row 1");
            expect($scope.newQuestion.answers[0].row).toEqual(true);

            expect($scope.newQuestion.answers[1].text).toEqual("Col 1");
            expect($scope.newQuestion.answers[1].row).toEqual(false);
        });
    });

    describe('duplicate question test', function() {
        it('checks duplication of grid question', function() {
            $scope.newQuestion = qList.initNewQuestion();
            $scope.newQuestion.ans_type = "grid";
            $scope.newQuestion.title = "new Question title";
            $scope.newQuestion.text = "new Question text";


            $scope.row_text = "Row 1";
            $scope.addNewGridText(true);
            $scope.col_text = "Col 1";
            $scope.addNewGridText(false);

            $scope.submitQuestion();
            question = qList.getQuestions()[0];

            $scope.duplicateQuestion(question);

            expect(qList.getQuestions()[0].id).toEqual(1);
            expect(qList.getQuestions()[1].id).toEqual(2);
            expect(qList.getQuestions()[0].answers).toEqual(qList.getQuestions()[1].answers);
            expect(qList.getQuestions()[0].title).toEqual(qList.getQuestions()[1].title);
            expect(qList.getQuestions()[0].text).toEqual(qList.getQuestions()[1].text);

        });
    });

});