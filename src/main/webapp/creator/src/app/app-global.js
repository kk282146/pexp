function getExperiment($http, onExperimentGet, expKey) {
    $http.get('/experiments/' + expKey)
        .success(onExperimentGet)
        .error(function(data, status, headers, config) {

            data = {
                "expId": 1650,
                "title": "Test experiment",
                "content": "{\"questions\": [{\"title\":\"Name\",\"text\":\"Jak sie nazywasz?\",\"id\":0,\"ans_type\":\"text\"},{\"title\":\"Name\",\"text\":\"Jak? O_o\",\"id\":6,\"ans_type\":\"text\",\"imageSrc\":\"http://www.psyscience.ru/wp-content/uploads/2013/12/surprise_man_face.jpg\"},{\"title\":\"Checkboxes\",\"text\":\"Pleć?\",\"id\":1,\"ans_type\":\"checkbox\",\"answers\":[{\"ans_no\":1,\"text\":\"mężczyzna\"},{\"ans_no\":2,\"text\":\"kobieta\"},{\"ans_no\":3,\"text\":\"nie wiem\"}]},{\"title\":\"Scale\",\"text\":\"Ile masz lat?\",\"id\":2,\"ans_type\":\"scale\",\"answers\":{\"0\":\"12\",\"1\":\"13\",\"2\":\"14\",\"3\":\"15\",\"4\":\"16\",\"5\":\"17\",\"6\":\"18\",\"7\":\"19\",\"8\":\"20\",\"9\":\"21\",\"ans_no\":\"10\"}},{\"title\":\"Gaps\",\"text\":\"The same color may have very different associations for different people. For me red color associates with [[red]], green<input type='button'>hi</input> color <b> indicates</b> [[green]] and blue is the color of [[blue]].\",\"id\":3,\"ans_type\":\"fillgaps\"}, {\"title\":\"List question\",\"text\":\"Ile masz dzieci?\",\"id\":4,\"ans_type\":\"list\", \"answers\":[{\"ans_no\":1,\"text\":\"jedno\"},{\"ans_no\":2,\"text\":\"dwa\"},{\"ans_no\":3,\"text\":\"wcale\"}]}, {\"title\":\"Date\",\"text\":\"Kiedy masz imieniny?\",\"ans_type\":\"date\",\"answers\":[{\"checked_year\":false,\"checked_time\":false}],\"id\":5}, {\"title\":\"Date + year\",\"text\":\"Kiedy sie urodziles?\",\"ans_type\":\"date\",\"answers\":{\"checked_year\":true,\"checked_time\":false},\"id\":6}, {\"title\":\"Date + year + time\",\"text\":\"Kiedy odlatuje Twoj samolot?\",\"ans_type\":\"date\",\"answers\":{\"checked_year\":true,\"checked_time\":true},\"id\":7}, {\"title\":\"Date (only time checked)\",\"text\":\"Kiedy masz egzamin z programowania?\",\"ans_type\":\"date\",\"answers\":{\"checked_year\":false,\"checked_time\":true},\"id\":8},{\"title\":\"Press asdf\",\"text\":\"Press any of keys a,s,d or f.\",\"ans_type\":\"keyboard\",\"answers\":{},\"id\":1,\"keyboardKeysAllowed\":\"asdf\"},{\"expositionTime\":0,\"answerTime\":0,\"answers\":{},\"edited\":false,\"id\":0,\"ans_type\":\"instructions\",\"title\":\"Końcowa gratulacja\",\"text\":\"Bla-bla-bla nie umiem pisać po-polsku!\"}]" +
                ", \"settings\": {\"navigate\": false} }",
                "shareKey": "0tfqt3"
            };
            onExperimentGet(data);
        });
}

function getUrlParam(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    return results == null ? null : results[1];
}
