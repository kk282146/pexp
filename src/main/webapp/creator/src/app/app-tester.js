var app = angular.module('ngBoilerplate', [
    'templates-app',
    'templates-common',
    'ui.router',
    'ngSanitize',
    'ngQuickDate',
    'ui.bootstrap'
])

    .config(function myAppConfig($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');
    })

    .run(function run() {
    })

    .controller('AppCtrl', function AppCtrl($scope, $location) {
        $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            if (angular.isDefined(toState.data.pageTitle)) {
                $scope.pageTitle = toState.data.pageTitle + ' | ngBoilerplate';
            }
        });
    });


app.controller('TesterController', ['$scope', '$http', '$sce', '$sanitize', function ($scope, $http, $sce, $sanitize) {
    $scope.jsn = null;
    $scope.count = 0;
    $scope.ans = null;
    var ans_json = null;
    $scope.myDate = new Date("2014-10-29T23:31:23Z");

    var onExperimentGet = function (data) {
        var content = JSON.parse(data.content);
        $scope.jsn = {
            'id': data['expId'],
            'title': data['title'],
            'questions': content.questions
        };
        $scope.count = $scope.jsn.questions.length;
        $scope.ans = new Array($scope.count);
        for (var i = 0; i < $scope.count; i ++ ) {
            $scope.ans[i] = null;
        }
        $scope.counter = -1;
        $scope.next();
    };

    getExperiment($http, onExperimentGet, getUrlParam("shareKey"));


    $scope.checkAns = function()
    {
        if (!$scope.question) {
            return false;
        }
        if (!$scope.question.required ||
            getAnswerTimeLimit() > 0 || //questions with limited time don't require answers
            $scope.question.ans_type == "instruction")
        {
            return true;
        }

        return parseAnswer().isAnswered;
    };


    //variable for the inner scope of text_tester view;
    var time;

    var parseAnswer = function() {
        var answer = {};
        var isAnswered = false;
        switch ($scope.ans_type) {
            case "text":
                answer['text'] = $scope.answer.text;
                isAnswered = Boolean($scope.answer.text);
                break;
            case "instructions":
                break;
            case "keyboard":
                answer['pressedKey'] = $scope.pressedKey;
                if ($scope.question.feedback) {
                    answer['correctAnswer'] = checkKey($scope.pressedKey, "correct");
                }
                isAnswered = checkKey($scope.pressedKey, "allowed");
                break;

            case "fillgaps":
                var gaps = document.getElementsByClassName("gaps");
                var filled = 0;
                for (var i=0; i<gaps.length; i++)
                {
                    if (Boolean(gaps[i].value)) {
                        filled++;
                    }
                    answer[gaps[i].name] = gaps[i].value;
                }
                isAnswered = (filled == gaps.length);
                break;

            case "checkbox":
                for (var j=0; j < $scope.question.answers.length; j++)
                {
                    var ans = $scope.question.answers[j];
                    // The answer is chosen.
                    if ($scope.selection.indexOf(ans['text']) > -1) {
                        answer[ans.text] = true;
                        isAnswered = true;
                    }
                    else
                    {
                        answer[ans.text] = false;
                    }
                }
                break;

            case "list":
                answer['chosen_option'] = $scope.selectedOption['text'];
                isAnswered = Boolean($scope.selectedOption['text']);
                break;

            case "scale":
                answer['chosen_option'] = $scope.selectedRadio;
                isAnswered = Boolean($scope.selectedRadio);
                break;

            case "radio":
                answer['chosen_option'] = $scope.radio;
                isAnswered = Boolean($scope.radio);
                break;

            case "date":
                if ($scope.checked_year && $scope.checked_time) {
                    answer['date'] = JSON.stringify($scope.dateTime);
                }
                if (!$scope.checked_year && $scope.checked_time) {
                    answer['date'] = JSON.stringify($scope.time);
                }
                if ($scope.checked_year && !$scope.checked_time) {
                    answer['date'] = JSON.stringify($scope.date);
                }
                if (!$scope.checked_year && !$scope.checked_time) {
                    //TODO: html for that case isn't ready
                    answer['date'] = JSON.stringify($scope.monthAndDay);
                    isAnswered = true;
                    break;
                }
                isAnswered = Boolean(answer['date']) && answer['date'] != "null";
                break;
            case "grid":

                answer = {};
                isAnswered = true;
                for (i = 0; i< $scope.grid_rows.length; i++)
                {
                    var row = $scope.grid_rows[i];
                    if (row.ans_no in $scope.grid)
                    {
                        answer[row.text] = $scope.grid[row.ans_no];
                    }
                    else
                    {
                        isAnswered = false;
                    }
                }

                break;

            default:
                isAnswered = true;
        }
        return {answer: answer, isAnswered:isAnswered};
    };

    /*saveCurrentAns reads input, converts and saves it
     Works only when 0 <= $scope.counter <= $scope.count - 1, otherwise does nothing*/
    $scope.saveCurrentAns = function () {
        if ((0 <= $scope.counter) && ($scope.counter <= $scope.count - 1)) {
            if ($scope.checkAns()) {
                var answer = parseAnswer().answer;

                if ($scope.question.reactionTime) {
                    answer['reactionTime'] = (new Date()).getTime() - time;
                }

                if ($scope.question.ans_type == "keyboard" && $scope.question.feedback) {
                    alert(answer['correctAnswer'] ? $scope.question.keyCorrectMsg : $scope.question.keyWrongMsg);
                }

                $scope.ans[$scope.counter] = answer;
                return true;
            }
        }
        return false;
    };

    function handleFillGapsHtml(questionHtml, answer) {
        var parsedAns;
        try {
            parsedAns = JSON.parse(answer);
        }
        catch(e)
        {
            parsedAns = {};
        }

        return questionHtml.replace(/\[\[(.*?)]]/g, function (match, capture) {
            var val = (parsedAns && parsedAns[capture]) ? parsedAns[capture] : "";

            return '<input type="text" class="input-xs gaps" name="' + capture +
                '" value="' + val + '" ' +
                 'oninput="fillGapChange()" />';
        });
    }


    $scope.selection = [];

    $scope.toggleSelection = function toggleSelection(ans_text) {
        var idx = $scope.selection.indexOf(ans_text);

        // is currently selected
        if (idx > -1) {
            $scope.selection.splice(idx, 1);
        }

        // is newly selected
        else {
            $scope.selection.push(ans_text);
        }
    };

    function setQuestion(counter) {
        $scope.questionHide = false;

        $scope.question = $scope.jsn.questions[counter];
        $scope.ans_type = $scope.question.ans_type;

        //removes all dangerous html tags
        var questionHtml = $sanitize($scope.question.text);

        switch ($scope.ans_type) {
            case "text":
                $scope.answer = { text : ""};
                break;
            case "fillgaps":
                questionHtml = handleFillGapsHtml(questionHtml, $scope.ans[counter]);
                break;

            case "checkbox":
                $scope.selection = [];
                $scope.q_answers = $scope.question.answers;
                break;

            case "list":
                $scope.options = $scope.question.answers;
                $scope.selectedOption = {};
                break;

            case "scale":
                $scope.scale_ans = $scope.question.answers;
                delete $scope.scale_ans["ans_no"];
                $scope.selectedRadio = "";
                break;

            case "date":
                $scope.checked_year = $scope.question.answers["checked_year"];
                $scope.checked_time = $scope.question.answers["checked_time"];

                if ($scope.checked_year && $scope.checked_time) {
                    $scope.dateTime = null;
                }
                if ($scope.checked_year && !$scope.checked_time) {
                    $scope.date = null;
                }

                if (!$scope.checked_year && $scope.checked_time) {
                    $scope.time = null;
                }

                if (!$scope.checked_year && $scope.checked_time) {
                    $scope.monthAndDay = null;
                }
                break;

            case "radio":
                $scope.radio = "";
                $scope.q_answers = $scope.question.answers;
                break;

            case "keyboard":
                $scope.pressedKey = '';
                break;
            case "grid":
                $scope.grid = {};
                var sort_func = function (a, b){return +(a.ans_no > b.ans_no);};
                $scope.grid_rows = $scope.question.answers.filter(function (val) {
                    return val.row;
                }).sort(sort_func);

                $scope.grid_cols = $scope.question.answers.filter(function (val) {
                    return !val.row;
                }).sort(sort_func);
                break;
        }

        $scope.question.html = $sce.trustAsHtml(questionHtml);

        window.clearTimeout($scope.expositionTimeout);
        window.clearTimeout($scope.answerTimeout);

        var expositionTime = 0;
        if ($scope.question.expositionTime) {
            expositionTime = parseInt($scope.question.expositionTime, 10);
            if (expositionTime > 0) {
                $scope.expositionTimeout = setTimeout(function () {
                    $scope.questionHide = true;
                    $scope.$digest();
                }, expositionTime * 1000);
            }
            else {
                expositionTime = 0;
            }
        }
        if (getAnswerTimeLimit() > 0) {
            $scope.answerTimeout = setTimeout(function () {
                $scope.next();
                $scope.$digest();
            }, (getAnswerTimeLimit() + expositionTime) * 1000);
        }
        time = (new Date()).getTime();
        showCurrentAns();
    }

    var getAnswerTimeLimit = function() {
        if ($scope.question && $scope.question.answerTime) {
            try {
                return parseInt($scope.question.answerTime, 10);
            }
            catch (e) {
            }
        }
        return 0;
    };

    $scope.questionHide = false;
    $scope.ans_type = "text";
    /*showCurrentAns has two roles - first it arranges display of "answers" section
     and second it reads and shows old answers (if returns to old questions are allowed).
     Works only when 0 <= $scope.counter <= $scope.count - 1, otherwise does nothing*/
    function showCurrentAns() {
        if ((0 <= $scope.counter) && ($scope.counter <= $scope.count - 1)) {
            var answer = $scope.ans[$scope.counter];

            $scope.showAnswerSection = true;

            switch ($scope.ans_type) {
                case "text":
                    $scope.answer.text = answer ? answer : "";
                    break;
                case "fillgaps": //is done already in handleFillGapsHtml
                case "instructions":
                case "keyboard":
                    $scope.showAnswerSection = false;
                    break;
            }
        }
    }

    //Works when $scope.counter + 1 < $scope.count, otherwise does nothing
    $scope.next = function () {
        if ($scope.counter + 1 < $scope.count) {
            $scope.counter += 1;

            setQuestion($scope.counter);
        }
    };

    $scope.checkQuestions = function () {
        if ($scope.ans == null) {
            return false;
        }
        return $scope.counter + 1 >= $scope.count;
    };
    $scope.saveAns = function () {
        if (!$scope.saveCurrentAns()) {
            return;
        }
        $scope.next();

    };

    $scope.answerToJson = function(question, answer) {
        var json = {};
        json['id'] = question.id;
        json['type'] = question.ans_type;
        json['answers'] = answer;
        return json;
    };

    $scope.submitAnswers = function () {
        $scope.ids = new Array($scope.count);
        var answers = [];
        for (var i = 0; i < $scope.count; i++) {
            if (jQuery.isEmptyObject($scope.ans[i])) {
            } else {
                    console.log($scope.ans[i]);
                    answers.push($scope.answerToJson($scope.jsn.questions[i], $scope.ans[i]));
            }
        }
        ans_json = {'experimentId': $scope.jsn.id, 'answers': JSON.stringify(answers)};

        console.log(ans_json);
        var res = $http.post('/replies/', ans_json);
        res.success(function (data, status, headers, config) {
            if (data) {
                document.location.href = "/experiments";
            } else {
                document.location.href = "/";
            }
        });
        res.error(function (data, status, headers, config) {
            alert("failure message: " + JSON.stringify({data: data}));
        });
    };

    var additionalKeysCodes = {
        37 : "left",
        38 : "up",
        39: "right",
        40: "down",
        13: "enter",
        32: "space"
    };

    function getKeyName(keyCode)
    {
        if (keyCode in additionalKeysCodes) {
            return additionalKeysCodes[keyCode];
        }
        else
        {
            return String.fromCharCode(keyCode).toLocaleLowerCase();
        }
        return null;
    }
    //check if key is allowed or correct
    //check parameter should be "allowed" or "correct"
    function checkKey(key_name, check)
    {
        if (key_name)
        {
            if (key_name.length == 1) {
                var keyboardKeys = check == "allowed" ?
                    $scope.question.keyboardKeysAllowed :
                    $scope.question.keyboardKeysCorrect;
                if (keyboardKeys.indexOf(key_name) > -1) {
                    return true;
                }
            } else if ($scope.question.additionalKeys) {
                var filtered = $scope.question.additionalKeys.filter(function (q) {
                    return (q.key == key_name);
                });
                if (filtered && filtered[0]) {
                    return filtered[0][check];
                }
            }
        }
        return false;
    }

    document.onkeyup = function(evt) {
        evt = evt || window.event;
        if ($scope.ans_type == "keyboard") {
            $scope.pressedKey = getKeyName(evt.keyCode);
            if (checkKey($scope.pressedKey, "allowed")) {
                if ($scope.ans[$scope.counter] == null) {
                    $scope.$apply($scope.saveAns());
                }
            }
        }
    };

}]);

function fillGapChange(){
    var scope = angular.element($("#TesterController")).scope();
    scope.$digest();
}
