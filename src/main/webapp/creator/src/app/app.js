var creatorApp = angular.module('creatorApp', ["ui.sortable", 'angularFileUpload']);

var CreatorMode = {
    EDIT: "edit",
    NEW: "new"
};

creatorApp.controller('CreatorController', ['$scope', '$http', 'questionsList', 'answersList', 'answerTypes', '$upload',
    function ($scope, $http, questionsList, answersList, answerTypes, $upload) {

        var expKey = getUrlParam("shareKey");
        var initScope = function() {
            $scope.newQuestion = questionsList.initNewQuestion();
            $scope.answerTypes = answerTypes.get();
            $scope.questions = questionsList.getQuestions();
            $scope.finalInstr = questionsList.getFinalInstructions();
        };

        if (expKey) //Edit mode
        {
            var onExperimentGet = function (data) {
                var content = JSON.parse(data.content);
                $scope.exp_title = data['title'];

                //moving final instructions to index 0
                questionsList.setQuestions(content.questions.slice(0, content.questions.length - 1));
                questionsList.setFinalInstructions(content.questions[content.questions.length - 1]);
                $scope.mode = CreatorMode.EDIT;
                initScope();
            };

            getExperiment($http, onExperimentGet, expKey);
        }
        else //New experiment mode
        {
            $scope.exp_title = '';
            $scope.asnwer = {};
            $scope.mode = CreatorMode.NEW;
            initScope();
        }

        var ans_no = 0;

        $scope.settingEvaluate = function (s) {
            if (s.value) {
                return "Yes";
            }
            return "No";
        };

        processDateQuestion = function(checked_year, checked_time) {
            $scope.newQuestion.answers = {};
            $scope.newQuestion.answers["checked_year"] = checked_year;
            $scope.newQuestion.answers["checked_time"] = checked_time;
        };

        processTimeQuestion = function(checked_duration) {
            $scope.newQuestion.answers["checked_duration"] = checked_duration;
        };

        $scope.uniquePositionName = function (question) {
        // This functions check if given position name is unique in experiment.

            // Test if exist other question with the same title.
            var index, len, q;
            for(index = 0, len = $scope.questions.length; index < len; ++index){
                q = $scope.questions[index];
                if(q.title == question.title){
                    if(q.id !== question.id){
                        return false;
                    }
                }
            }
            return true;
        };

        $scope.submitQuestion = function () {
            var question = $scope.newQuestion;
            if ((!question.text && !question.imageSrc) || !question.title) {
                $scope.alertShow = true;
                return false;
            }
            if(!$scope.uniquePositionName(question)){
                $scope.alertShow2 = true;
                return false;
            }

            if(question.ans_type == "date") {
                processDateQuestion($scope.checked_year, $scope.checked_time);
            }

            if(question.ans_type == "time")  {
                processTimeQuestion($scope.checked_duration);
            }

            if(question.edited) {
                questionsList.updateQuestion(question);
            }
            else {
                questionsList.postQuestion(question);
            }
            $scope.questions = questionsList.getQuestions();
            $scope.finalInstr = questionsList.getFinalInstructions();
            $scope.newQuestion = null;
            answersList.deleteAll();
            $scope.questionShow = false;
            $scope.showTab('tab_questions');
        };

        $scope.showTab = function(tabId) {
            var a = $('.nav-pills a[href="#' + tabId + '"]').tab('show');
        };

        $scope.deleteQuestion = function (question) {
            questionsList.removeQuestion(question);
            $scope.questions = questionsList.getQuestions();
        };

        $scope.range = function (min, max, step) {
            step = step || 1;
            var input = [];
            for (var i = min; i <= max; i += step) {
                input.push(i);
            }
            return input;
        };

        //prepare question to editing: clone it and added all default values to show it properly in the editor
        var cloneQuestion = function (question) {
            var q = jQuery.extend(true, {}, question);
            if (!q.expositionTime) {
                q.expositionTime = 0;
            }
            if (!q.answerTime) {
                q.answerTime = 0;
            }
            return q;
        };

        $scope.editQuestion = function (question) {
            $scope.showQuestion(question.ans_type);
            $scope.alertShow = false;
            $scope.alertShow2 = false;

            //deep copy
            $scope.newQuestion = cloneQuestion(question);

            $scope.newQuestion.edited = true;
            if($scope.newQuestion.ans_type == "date") {
                $scope.checked_year = $scope.newQuestion.answers["checked_year"];
                $scope.checked_time = $scope.newQuestion.answers["checked_time"];
            }

            if($scope.newQuestion.ans_type == "time")  {
                $scope.checked_duration = $scope.newQuestion.answers["checked_duration"];
            }

            if($scope.newQuestion.ans_type == "keyboard")  {
                if (!$scope.newQuestion.keyWrongMsg && !$scope.newQuestion.keyCorrectMsg) {
                    $scope.newQuestion.keyWrongMsg = KEY_WRONG_MSG;
                    $scope.newQuestion.keyCorrectMsg = KEY_CORRECT_MSG;
                }
            }

            $scope.imageShow = Boolean(question.imageSrc);
            $scope.imageUploaded = $scope.imageShow;
            for (var i = 0; i < question.answers.length; i++) {
                answersList.postAnswer(question.answers[i]);
            }
            //if we will call showTab now, it will press
            $scope.showTab('tab_' + $scope.newQuestion.ans_type + '_question');
        };

        $scope.duplicateQuestion = function (question) {
            questionsList.duplicateQuestion(question);
            $scope.questions = questionsList.getQuestions();
        };

        var KEY_WRONG_MSG = "Niepoprawna odpowiedź";
        var KEY_CORRECT_MSG = "OK";
        $scope.showQuestion = function (ansType) {
            $scope.setAnsType(ansType);
            $scope.alertShow = false;
            $scope.alertShow2 = false;
            $scope.questionShow = true;
            $scope.imageUploaded = Boolean($scope.imageSrc);
            $scope.imageShow = $scope.imageUploaded;
            if($scope.newQuestion.ans_type == "keyboard")  {
                $scope.newQuestion.keyWrongMsg = KEY_WRONG_MSG;
                $scope.newQuestion.keyCorrectMsg = KEY_CORRECT_MSG;
                $scope.newQuestion.additionalKeys = [
                    {key:"left", text:"←", allowed: false, correct: false},
                    {key:"up", text:"↑", allowed: false, correct: false},
                    {key:"right", text:"→", allowed: false, correct: false},
                    {key:"down", text:"↓", allowed: false, correct: false},
                    {key:"space", text:"Spacja", allowed: false, correct: false},
                    {key:"enter", text:"Enter", allowed: false, correct: false}
                ];
            }
            $scope.focusPosInput=true;
        };

        //Ans type
        $scope.setAnsType = function (ansType) {
            if (!ansType) {
                return;
            }
            $scope.answerTypeTitle = answerTypes.getDictionary()[ansType].title;
            $scope.newQuestion = questionsList.initNewQuestion();
            $scope.newQuestion.ans_type = ansType;
            answersList.deleteAll();
            $scope.answer = {};
        };

        $scope.addNewGridText = function (isRow) {
            ans_no = ans_no + 1;
            $scope.answer = {};
            $scope.answer.ans_no = ans_no;
            $scope.answer.row = isRow;
            if (isRow) {
                $scope.answer.text = $scope.row_text;
            } else {
                $scope.answer.text = $scope.col_text;
            }
            answersList.postAnswer($scope.answer);
            $scope.newQuestion.answers = answersList.getAnswers();
            $scope.row_text = "";
            $scope.col_text = "";
        };


        //Checkbox
        $scope.addNewCheckBoxAnswer = function () {
            ans_no = ans_no + 1;
            $scope.answer.ans_no = ans_no;
            answersList.postAnswer($scope.answer);
            $scope.newQuestion.answers = answersList.getAnswers();
            $scope.answer = {};
        };

        $scope.removeCheckBoxInput = function (answer) {
            answersList.removeAnswer(answer);
            $scope.newQuestion.answers = answersList.getAnswers();
        };


        //Scale question
        $scope.createEmptyAnswers = function () {
            for (var i=0; i < $scope.newQuestion.answers.ans_no; i++){
                $scope.newQuestion.answers[i] = i + 1;
            }
        };

        //Submit survey
        $scope.submitSurvey = function () {
            var experiment = {
                shareKey: expKey,
                title: $scope.exp_title,
                content: angular.toJson(
                    {
                        questions: questionsList.getQuestions().concat(questionsList.getFinalInstructions())
                    })
            };
            var url = ($scope.mode == CreatorMode.EDIT) ?
                '/experiments/update' :
                '/experiments/';
            var res = $http.post(url, experiment);
            res.success(function (data, status, headers, config) {
                document.location.href = "/experiments/";
            });
            res.error(function (data, status, headers, config) {
                if (status == 403)//unautorised, session is expired
                {
                    $("#loginModal").modal("show");
                    $scope.login_callback = $scope.submitSurvey;
                    //I use a hack there: if user try to login using this modal form and he is failed
                    //then he will return back here, so I have to show "login failed" message
                    $scope.account = {
                        login_failed_msg:
                            (Boolean)($scope.login_attempt_time && ((new Date() - $scope.login_attempt_time) < 30000))//30 seconds
                    };
                }
            });
        };

        $scope.submit_login = function(callback){
            if ($scope.account.username && $scope.account.password)
            {
                $scope.login_attempt_time = new Date();
                var res = $http({
                    method: 'POST',
                    url: "/login",
                    data: $.param($scope.account),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });

                res.success(function (data, status, headers, config) {
                    if ($scope.login_callback) {
                        $scope.login_callback();
                    }
                });
                res.error(function (data, status, headers, config) {
                    alert("Logowanie sie nie udało, sprobuj ponownie.");
                });
            }
        };

        $scope.$watch('images', function () {
            $scope.upload($scope.images);
        });

        $scope.upload = function (images) {
            if (images && images.length) {
                var file = images[0];
                $upload.upload({
                    url: '/experiments/uploadImage/' + (expKey ? expKey : "null") + "/" + questionsList.getQuestionId(),
                    file: file
                }).success(function (data, status, headers, config) {
                    expKey = data['shareKey'];
                    $scope.newQuestion.imageSrc = '/img/user/' + data['fileName'];
                    $scope.imageUploaded = true;
                });
            }
        };

        $scope.removeImage = function () {
            $scope.imageShow = false;
            $scope.newQuestion.imageSrc = null;
            $scope.imageUploaded = false;
        };

        $scope.insertGap = function () {
            $scope.newQuestion.text =
                Boolean($scope.newQuestion.text) ?
                    ($scope.newQuestion.text + "[[nazwij_lukę]]") : "[[nazwij_lukę]]";
        };

        $scope.onCheckboxEnter = function (event) {
            if(event.which === 13) {
                $scope.addNewCheckBoxAnswer();
                event.preventDefault();
            }
        };

        $scope.onTitleEnter = function (event) {
            if(event.which === 13) {
                event.preventDefault();
            }
        };

        // Return a helper with preserved width of cells
        var fixHelper = function(e, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui.clone();//clone used to prevent firing "click" event in Firefox
        };

        $scope.qsSortable = {
            items: '> tr',
            //containment: "parent",//Removed as it prevents multiline questions to be dragged to the last position
            cursor: "move",//Change the cursor icon on drag
            tolerance: "pointer",//Read http://api.jqueryui.com/sortable/#option-tolerance
            placeholder:'sortable-placeholder',
            update: function( event, ui ) {
                questionsList.setQuestions($scope.questions);
            },
            //fix the width of placeholder
            start: function (event, ui) {
                // In some weird reason colspan should be 1 larger that actual
                ui.placeholder.html('<td colspan="4" width="100%">&nbsp;</td>');
            },
            helper: fixHelper
        };
    }]);

creatorApp.factory('questionsList', function () {
    var factory = {};
    var questions = [];

    factory.initNewQuestion = function() {
        ans_no = 0;
        return {
            expositionTime: 0,
            answerTime: 0,
            answers: {},
            edited: false,
            required: true
        };
    };

    var createFinalInstructions = function() {
        var fi = factory.initNewQuestion();
        fi.id = 0;
        fi.ans_type = 'instructions';
        fi.title = 'Instrukcja końcowa';
        fi.text = 'Dziękujemy za udział w eksperymencie.';
        return fi;
    };

    var finalInstr = createFinalInstructions();
    var curQuestionId = 1;

    factory.setQuestions = function(questionsList) {
        questions = questionsList;
        if (curQuestionId < questions.length + 1) {
            curQuestionId = questions.length + 1;
        }
    };

    factory.getQuestionId = function() {
        return curQuestionId;
    };

    factory.getFinalInstructions = function () {
        return finalInstr;
    };
    factory.setFinalInstructions = function (val) {
        finalInstr = val;
    };

    factory.getQuestions = function () {
        return questions;
    };

    //remove all unnecessary staff added by Angular and another code.
    var cleanUpQuestion = function (question) {
        var q = {};
        q.id = question.id;
        q.title = question.title;
        q.text = question.text;
        q.ans_type = question.ans_type;
        q.answers = question.answers;
        if (question.imageSrc) {
            q.imageSrc = question.imageSrc;
        }
        if (question.expositionTime){
            q.expositionTime = question.expositionTime;
        }
        if (question.answerTime){
            q.answerTime = question.answerTime;
        }
        if (question.reactionTime){
            q.reactionTime = question.reactionTime;
        }
        if (question.required){
            q.reactionTime = question.required;
        }
        if (question.ans_type == "keyboard") {
            q.keyboardKeysAllowed = question.keyboardKeysAllowed;
            q.additionalKeys = question.additionalKeys;
            if (question.feedback) {
                q.feedback = true;
                q.keyboardKeysCorrect = question.keyboardKeysCorrect;
                q.keyCorrectMsg = question.keyCorrectMsg;
                q.keyWrongMsg = question.keyWrongMsg;
            }
        }
        return q;
    };

    factory.postQuestion = function (question) {
        var cleaned = cleanUpQuestion(question);
        cleaned.id = curQuestionId;
        questions.push(cleaned);
        curQuestionId = curQuestionId + 1;
    };

    var getQuestionIndex = function (question) {
        var filtered = questions.filter(function(q) {
            return (q.id === question.id);
        });
        if (filtered && filtered[0]) {
            return questions.indexOf(filtered[0]);
        }
        else {
            return -1;
        }
    };

    factory.updateQuestion = function (question) {
        var clone = cleanUpQuestion(question);
        var index = getQuestionIndex(clone);
        if (index > -1) {
            questions[index] = clone;
        }
        else if (question.id === 0)//final instruction
        {
            factory.setFinalInstructions(clone);
        }
    };

    factory.removeQuestion = function (question) {
        var index = getQuestionIndex(question);
        if (index > -1) {
            questions.splice(index, 1);
        }
    };

    factory.duplicateQuestion = function (question) {
        var index = getQuestionIndex(question);
        if (index > -1) {
            //deep copy of question
            var copy = jQuery.extend(true, {}, question);
            copy.id = curQuestionId;
            questions.splice(index + 1, 0, cleanUpQuestion(copy));
            curQuestionId = curQuestionId + 1;
        }
    };

    return factory;
});

creatorApp.factory('answersList', function () {
    var answers = [];
    var factory = {};

    factory.getAnswers = function () {
        return answers;
    };

    factory.postAnswer = function (answer) {
        answers.push(
            {
                ans_no: answer.ans_no,
                text: answer.text,
                row: answer.row
            });
    };

    factory.removeAnswer = function (answer) {
        pos = answers.map(function(e) { return e.ans_no; }).indexOf(answer.ans_no);
        answers.splice(pos, 1);
    };

    factory.deleteAll = function () {
        answers = [];
    };
    return factory;
});

creatorApp.factory('answerTypes', function () {
    var factory = {};
    var types =
        [
            { type : "instructions", title: "Instrukcja"},
            { type : "text", title:  "Tekst"},
            { type : "checkbox", title:  "Wielokrotny wybór"},
            { type : "radio", title:  "Jednokrotny wybór"},
            { type : "list", title:  "Lista"},
            { type : "scale", title:  "Skala"},
            { type : "grid", title:  "Siatka"},
            { type : "date", title:  "Data"},
            { type : "time", title:  "Czas"},
            { type : "keyboard", title:  "Klawiatura"},
            { type : "fillgaps", title:  "Wypełnianie luk"}
        ];
    var typesDict = {};
    for (var i=0; i < types.length; i++){
        var type = types[i];
        typesDict[type.type] = type;
    }

    factory.get = function() {
        return types;
    };

    factory.getDictionary = function() {
        return typesDict;
    };

    return factory;
});

creatorApp.directive('focusMe', function($timeout) {
    return {
        scope: { trigger: '=focusMe' },
        link: function(scope, element) {
            scope.$watch('trigger', function(value) {
                if(value === true) {
                    element[0].focus();
                    scope.trigger = false;
                }
            });
        }
    };
});