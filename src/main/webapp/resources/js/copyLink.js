var client = new ZeroClipboard($(".copy-button"));

client.on( "ready", function( readyEvent ) {
    // alert( "ZeroClipboard SWF is ready!" );

    client.on( 'copy', function(event) {
        var id = event.target.id.replace("but_", "");
        var a_tag = document.getElementById("link_" + id);
        event.clipboardData.setData('text/plain', a_tag.href);
    } );

    client.on( "aftercopy", function( event ) {
        // `this` === `client`
        // `event.target` === the element that was clicked
        alert("Link " + event.data["text/plain"] + " został skopiowany do schowka." );
    } );
} );
