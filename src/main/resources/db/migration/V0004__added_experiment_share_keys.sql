DELETE FROM replies;
DELETE FROM experiments;

ALTER TABLE public.experiments ADD COLUMN share_key CHAR(6) NOT NULL UNIQUE DEFAULT 'aaaaaa';

CREATE UNIQUE INDEX share_key_idx ON public.experiments (share_key);