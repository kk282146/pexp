CREATE TABLE replies
(
  id SERIAL PRIMARY KEY NOT NULL,
  experiment_id INT NOT NULL,
  answers text NOT NULL
);
ALTER TABLE replies
ADD FOREIGN KEY (experiment_id) REFERENCES experiments(id);