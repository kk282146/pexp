CREATE TABLE accounts
(
  id SERIAL PRIMARY KEY NOT NULL,
  firstname VARCHAR(120),
  lastname VARCHAR(120),
  email VARCHAR(120) NOT NULL,
  password VARCHAR(70) NOT NULL
);
ALTER TABLE accounts ADD CONSTRAINT unique_id UNIQUE (id);
ALTER TABLE accounts ADD CONSTRAINT unique_email UNIQUE (email);

CREATE TABLE experiments
(
  id SERIAL PRIMARY KEY NOT NULL,
  title VARCHAR(120) NOT NULL,
  author_id INT NOT NULL,
  content text NOT NULL
);
ALTER TABLE experiments
ADD FOREIGN KEY (author_id) REFERENCES accounts (id);