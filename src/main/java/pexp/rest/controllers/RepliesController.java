package pexp.rest.controllers;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import pexp.core.models.entities.Experiment;
import pexp.core.models.entities.Replies;
import pexp.core.services.ExperimentService;
import pexp.core.services.RepliesService;
import pexp.core.services.exceptions.ExperimentNotFoundException;
import pexp.rest.exceptions.NotFoundException;
import pexp.rest.resources.RepliesResource;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("/replies")
public class RepliesController extends BaseController {
    @Autowired
    RepliesService service;

    @Autowired
    ExperimentService expService;

    @RequestMapping(value="/{sharedKey}",
            method = RequestMethod.GET)
    @PreAuthorize("isAuthenticated()")
    public String getResultsView (ModelMap model, @PathVariable String sharedKey) {
        Map<String, Object> map = getResults(sharedKey);
        model.addAttribute("names", map.get("names"));
        model.addAttribute("widths", map.get("widths"));
        model.addAttribute("type", map.get("types"));
        model.addAttribute("labels", map.get("labels"));
        model.addAttribute("replies", map.get("replies"));
        return "replies";
    }

    private Map<String, Object> getResults(String sharedKey) {

        // Download experiment.
        Experiment exp = expService.findByShareKey(sharedKey);

        // Set for names of questions.
        TreeSet<String> names = new TreeSet<>();

        // Map for widths of questions column (number of answers).
        TreeMap<String, Integer> widths = new TreeMap<>();

        //Map for types of questions.
        TreeMap<String, String> type = new TreeMap<>();

        // Map for labels of simple answers to questions.
        TreeMap<String, ArrayList<String>> labels = new TreeMap<>();

        // Array for single answers to questions for every user.
        ArrayList<TreeMap<String, TreeMap<String, String>>> replies = new ArrayList<>();

        // Assistant array for label creation.
        ArrayList<String> label;

        // Assistant array for question name.
        String name;

        name = "Czas oddania";
        names.add(name);
        type.put(name, "Czas");
        widths.put(name, 1);
        label = new ArrayList<>();
        label.add("Zarejestrowany czas");
        labels.put(name, label);

        name = "Data oddania";
        names.add(name);
        type.put(name, "Data");
        widths.put(name, 1);
        label = new ArrayList<>();
        label.add("Zarejestrowana data");
        labels.put(name, label);

        // Downloading all replies for question.
        List<Replies> repliesList = service.findRepliesByExperiment(exp.getId());

        ObjectMapper mapper = new ObjectMapper();
        JsonFactory factory = mapper.getFactory();

        try {
            JsonParser jp = factory.createParser(exp.getContentAsStirng());
            JsonNode j = mapper.readTree((jp));
            JsonNode questions = j.findValue("questions");

            // Map that will help assign answers to question by question id.
            Map<String, String> dictionary = new TreeMap<>();
            for (JsonNode question : questions) {
                String title = question.findValue("title").toString();
                String id = question.findValue("id").toString();
                dictionary.put(id, title);

                names.add(title);
                type.put(title, question.findValue("ans_type").toString());
            }

            // Assistant maps - answer for all answers of user and simple answer for answers for single question.
            TreeMap<String, TreeMap<String, String>> answer;
            TreeMap<String, String> singleAnswer;

            for (Replies r : repliesList) {

                answer = new TreeMap<>();


                // Getting date and time of every replies.
                Date d = new Date(r.getTimeStamp().getTime());
                String data = new SimpleDateFormat("dd/mm/yyyy").format(d);
                singleAnswer = new TreeMap();
                singleAnswer.put("Zarejestrowana data", data);
                answer.put("Data oddania", singleAnswer);

                Time t = new Time(r.getTimeStamp().getTime());
                singleAnswer = new TreeMap<>();
                singleAnswer.put("Zarejestrowany czas", t.toString());
                answer.put("Czas oddania", singleAnswer);

                // Getting all other answers to replies.
                jp = factory.createParser(r.getAnswers());
                JsonNode agentAnswers = mapper.readTree(jp);

                for (JsonNode reply : agentAnswers) {
                    name = dictionary.get(reply.findValue("id").toString());

                    JsonNode ans = reply.findValue("answers");

                    // Testing whether we already add this question labels.
                    if (! labels.containsKey(name)) {
                        // If not we add all labels.
                        label = new ArrayList<>();
                        Iterator<String> namesIter = ans.fieldNames();
                        while (namesIter.hasNext())
                            label.add(namesIter.next());
                        labels.put(name, label);
                        widths.put(name, label.size());
                    }

                    // Adding answers value.
                    singleAnswer = new TreeMap();
                    Iterator<Map.Entry<String, JsonNode>> fields = ans.fields();
                    while (fields.hasNext()) {
                        Map.Entry<String, JsonNode> field = fields.next();
                        singleAnswer.put(field.getKey(), field.getValue().asText());
                    }

                    answer.put(name, singleAnswer);
                }

                replies.add(answer);
            }

            TreeSet<String> new_list = new TreeSet<>();
            for (String n : names)
                if (labels.containsKey(n))
                    new_list.add(n);

            names = new_list;
            /*
                } catch (Exception exceptio) {
                    name = "Error";
                    questionsNames.add(name);
                    TreeMap<String, String> err = new TreeMap<>();
                    err.put(name, exceptio.toString());
                    TreeMap<String, TreeMap<String, String>> tmp = new TreeMap<>();
                    tmp.put(name, err);
                    answers.add(tmp);
                    questionsData.put(name, new QuestionData(2, name));
                }
            }*/
        } catch (Exception e) {

        }
        Map<String, Object> map = new HashMap<>();
        map.put("names", names);
        map.put("widths", widths);
        map.put("types", type);
        map.put("labels", labels);
        map.put("replies", replies);
        return map;
    }

    @RequestMapping(value="/xls/{sharedKey}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @PreAuthorize("isAuthenticated()")
    @ResponseBody
    public void getXls (@PathVariable String sharedKey, HttpServletResponse response) {

        Map<String, Object> model = getResults(sharedKey);

        // Set for names of questions.
        TreeSet<String> names = (TreeSet<String>)model.get("names");

        // Map for widths of questions column (number of answers).
        TreeMap<String, Integer> widths = (TreeMap<String, Integer>)model.get("widths");

        //Map for types of questions.
        TreeMap<String, String> types = (TreeMap<String, String>)model.get("types");

        // Map for labels of simple answers to questions.
        TreeMap<String, ArrayList<String>> labels = (TreeMap<String, ArrayList<String>>)model.get("labels");

        // Array for single answers to questions for every user.
        ArrayList<TreeMap<String, TreeMap<String, String>>> replies =
                (ArrayList<TreeMap<String, TreeMap<String, String>>>)model.get("replies");

        HSSFWorkbook wb = new HSSFWorkbook();
        // create a new sheet
        HSSFSheet s = wb.createSheet();
        // declare a row object reference
        HSSFRow row;
        // declare a cell object reference
        HSSFCell c;
        HSSFCellStyle cs;

        HSSFFont f = wb.createFont();

        // Define a few rows
        short rownum = 0;
        int cellnum;

        for (int r = 0; r < replies.size() + 3; r++) {
            row = s.createRow(r);
            cellnum = 0;
            for (String name : names) {
                c = row.createCell(cellnum);
                if (widths.get(name) > 1) {
                    c.setCellStyle(createStyle(wb, true, false));
                    for (int j = 1; j < widths.get(name) - 1; j++) {
                        c = row.createCell(cellnum + j);
                        c.setCellStyle(createStyle(wb, false, false));
                    }
                    cellnum+=widths.get(name);
                    c = row.createCell(cellnum-1);
                    c.setCellStyle(createStyle(wb, false, true));
                }
                else
                {
                    c.setCellStyle(createStyle(wb, true, true));
                    cellnum++;
                }
            }
        }

        f.setColor(HSSFColor.WHITE.index);
        f.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

        cellnum = 0;
        for (String name : names) {
            c = s.getRow(rownum).getCell(cellnum);
            c.setCellValue(name);

            for (int j = 0; j<widths.get(name); j++)
            {
                c = s.getRow(rownum).getCell(cellnum + j);
                cs = c.getCellStyle();
                cs.setFont(f);
                cs.setFillForegroundColor(HSSFColor.ROYAL_BLUE.index);
                cs.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            }
            cellnum+=widths.get(name);
        }
        rownum++;

        f = wb.createFont();
        f.setItalic(true);

        cellnum = 0;
        for (String name : names) {
            c = s.getRow(rownum).getCell(cellnum);

            c.setCellValue(types.get(name));

            for (int j = 0; j<widths.get(name); j++)
            {
                c = s.getRow(rownum).getCell(cellnum + j);

                cs = c.getCellStyle();
                cs.setFont(f);
                cs.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                cs.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            }
            cellnum+=widths.get(name);
        }
        rownum++;

        cellnum = 0;
        for (String name : names) {
            for (String label : labels.get(name)) {
                c = s.getRow(rownum).getCell(cellnum);

                c.setCellValue(label);

                cs = c.getCellStyle();
                cs.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                cs.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                cellnum++;
            }
        }
        rownum++;

        for (TreeMap<String, TreeMap<String, String>> rep : replies) {
            cellnum = 0;
            for (String name : names) {
                for (String label : labels.get(name)) {
                    TreeMap<String, String> quest_repl = rep.get(name);
                    if (quest_repl != null) {
                        String repl = quest_repl.get(label);
                        c = s.getRow(rownum).getCell(cellnum);
                        c.setCellValue(repl != null ? repl : " ");
                        cs = c.getCellStyle();
                        cs.setFillForegroundColor(HSSFColor.WHITE.index);
                    }
                    cellnum++;
                }
            }
            rownum++;
        }

        rownum--;
        for (int i =0; i < cellnum; i++) {
            s.setColumnWidth(i, 4500);
            c = s.getRow(rownum).getCell(i);
            cs = c.getCellStyle();
            cs.setBorderBottom(CellStyle.BORDER_THIN);
        }

        try {

            Experiment exp = expService.findByShareKey(sharedKey);

            String fileName = exp.getTitle() + "_replies.xls";

            // set content attributes for the response
            response.setContentType("application/vnd.ms-excel");

            // set headers for the response
            String headerKey = "Content-Disposition";
            String headerValue = String.format("attachment; filename=\"%s\"",
                    fileName);
            response.setHeader(headerKey, headerValue);

            // get output stream of the response
            OutputStream outStream = response.getOutputStream();

            wb.write(outStream);
            outStream.close();
        } catch (IOException e) {
            //TODO handle IOException
        }
    }

    private HSSFCellStyle createStyle(HSSFWorkbook wb, Boolean hasLeftBorder, Boolean hasRightBorder)
    {
        HSSFCellStyle cs = wb.createCellStyle();
        if (hasLeftBorder)
            cs.setBorderLeft(CellStyle.BORDER_THIN);
        if (hasRightBorder)
            cs.setBorderRight(CellStyle.BORDER_THIN);
        return cs;
    }

    //returns true if user is logged in
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Boolean> create(@RequestBody RepliesResource resource){
        try {
            service.saveReplies(resource.getExperimentId(), resource.toReplies());

            HttpHeaders headers = new HttpHeaders();
            return new ResponseEntity<>(getLoggedInUser() != null, headers, HttpStatus.CREATED);
        }
        catch (ExperimentNotFoundException exception)
        {
            throw new NotFoundException(exception);
        }
    }
}