package pexp.rest.controllers;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pexp.core.models.entities.Experiment;
import pexp.core.services.ExperimentService;
import pexp.core.services.exceptions.AccountDoesNotExistException;
import pexp.core.services.exceptions.ExperimentExistsException;
import pexp.core.services.exceptions.ExperimentNotFoundException;
import pexp.rest.exceptions.ConflictException;
import pexp.rest.exceptions.ForbiddenException;
import pexp.rest.exceptions.NotFoundException;
import pexp.rest.resources.ExperimentResource;
import pexp.rest.resources.asm.ExperimentResourceAsm;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Controller
@RequestMapping("/experiments")
public class ExperimentController extends BaseController {

    @Autowired
    private ExperimentService service;

    private static Logger log = Logger.getLogger(ExperimentController.class.getName());

    @RequestMapping(value="/{shareKey}", method = RequestMethod.GET)
    @ResponseBody   
    public ExperimentResource getExperiment(@PathVariable String shareKey) {
        Experiment exp = RestPreconditions.checkFound(service.findByShareKey(shareKey));
        return new ExperimentResourceAsm().toResource(exp);
    }

    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/uploadImage/{shareKey}/{questionId}")
    public ResponseEntity<String> uploadImage(@RequestParam("file") MultipartFile image,
                                              @PathVariable String shareKey,
                                              @PathVariable int questionId) throws IOException {

        if(getLoggedInUser() != null) {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            try {
                if (image.isEmpty())
                    return new ResponseEntity<>("Image shouldn't be empty", headers, HttpStatus.BAD_REQUEST);
                
                try {
                    validateImage(image);
                }
                catch(RuntimeException re)
                {
                    return new ResponseEntity<>(re.getMessage(), headers, HttpStatus.BAD_REQUEST);
                }
                if (shareKey.length() != Experiment.SHARE_KEY_LEN || shareKey.contains("."))//new experiment
                    shareKey = service.generateUniqueExperimentKey();
                else {
                    Experiment exp = service.findByShareKey(shareKey);
                    if (exp != null && !exp.getAuthor().getId().equals(getLoggedInUser().getId()))
                        throw new ForbiddenException();
                }

                String fileName = shareKey + "/" + questionId + '/' + RandomStringUtils.random(6, true, true).toLowerCase() + "." + image.getContentType().substring("image/".length());
                saveImage(fileName, image);
                return new ResponseEntity<>("{\"shareKey\":\"" + shareKey + "\",\"fileName\":\""+ fileName + "\"}", headers, HttpStatus.OK);
            }           
            catch (IOException e){
                return new ResponseEntity<>("Error trying to save the file: " + e.getMessage(), headers, HttpStatus.BAD_REQUEST);
            }
        } else {
            throw new ForbiddenException();
        }
    }

    private void validateImage(MultipartFile image) {
        if (!image.getContentType().startsWith("image/")) {
            throw new RuntimeException("Only images are accepted");
        }
    }

    private void saveImage(String filename, MultipartFile image)
            throws RuntimeException, IOException
    {
        File file = new File(servletContext.getRealPath("/") + "/resources/img/user/"
                + filename);

        FileUtils.writeByteArrayToFile(file, image.getBytes());
        System.out.println("Go to the location:  " + file.toString()
                + " on your computer and verify that the image has been stored.");
    }

    @RequestMapping(value="/remove/{shareKey}", method = RequestMethod.GET)
    @PreAuthorize("isAuthenticated()")
    public String confirmRemove(@PathVariable("shareKey") String shareKey, ModelMap model)
    {
        if(getLoggedInUser() != null) {
            Experiment exp = service.findByShareKey(shareKey);
            if (exp != null) {
                model.addAttribute("experiment", exp);
                return "removeExperiment";
            }
        }

        return "redirect:/experiments";
    }

    @RequestMapping(value="/remove/{shareKey}", method = RequestMethod.POST)
    @PreAuthorize("isAuthenticated()")
    public String remove(@PathVariable("shareKey") String shareKey)
    {
        if(getLoggedInUser() != null)
            try {
                service.deleteExperiment(getLoggedInUser().getId(), shareKey);
            }
            catch(AccountDoesNotExistException exception)
            {
                log.log(Level.WARNING, "Logged user with id {0} not exists. It is very strange.",
                        getLoggedInUser().getId());
            }
            catch(ExperimentNotFoundException exception) {
                log.info(String.format("User %d tried to delete not existing experiment with shareKey '%s'.",
                        getLoggedInUser().getId(), shareKey));
            }
            catch(ForbiddenException exception) {
                log.warning(String.format(
                        "User %d wasn't able to delete the experiment with shareKey '%s' because he don't have such permissions.",
                        getLoggedInUser().getId(), shareKey));
            }
        return "redirect:/experiments";
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<ExperimentResource> create(@RequestBody ExperimentResource resource){
        if(getLoggedInUser() != null) {
            try {
                Experiment experiment = service.saveExperiment(getLoggedInUser().getId(), resource.toExperiment());

                ExperimentResource res = new ExperimentResourceAsm().toResource(experiment);
                HttpHeaders headers = new HttpHeaders();
                return new ResponseEntity<>(res, headers, HttpStatus.CREATED);
            }
            catch(AccountDoesNotExistException exception)
            {
                throw new NotFoundException(exception);
            }
            catch(ExperimentExistsException exception) {
                throw new ConflictException(exception);
            }

        } else {
            throw new ForbiddenException();
        }
    }

    @RequestMapping(method = RequestMethod.POST, value="/update")
    @ResponseBody
    public ResponseEntity<ExperimentResource> update(@RequestBody ExperimentResource resource){
        if(getLoggedInUser() != null) {
            try {
                service.updateExperiment(getLoggedInUser().getId(), resource.toExperiment());

                HttpHeaders headers = new HttpHeaders();
                return new ResponseEntity<>(null, headers, HttpStatus.OK);
            }
            catch(AccountDoesNotExistException | ExperimentNotFoundException exception)
            {
                throw new NotFoundException(exception);
            }
        } else {
            throw new ForbiddenException();
        }
    }

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("isAuthenticated()")
    public String getExperimentsByAuthor(ModelMap model) {
        if(getLoggedInUser() != null) {
            try
            {
                Iterable<Experiment> experimentList = service.findExperimentsByAuthor(getLoggedInUser().getId());
                final List<ExperimentResource> experimentListRes = new ArrayList<>();
                for(Experiment ex :experimentList) {
                   experimentListRes.add(new ExperimentResourceAsm().toResource(ex));
                }

                model.addAttribute("experiments", experimentListRes);
                return "experiments";
            }
            catch (AccountDoesNotExistException exception)
            {
                return "redirect:/login";
            }
        } else {
            return "redirect:/login";
        }
    }
}
