package pexp.rest.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class IndexController {
	@RequestMapping(method = RequestMethod.GET)
	public String index() {
		return "index";
	}

    @RequestMapping(method = RequestMethod.HEAD)
    public String index_head() {
        return "index";
    }
}