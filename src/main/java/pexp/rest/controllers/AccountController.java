package pexp.rest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pexp.core.models.entities.Account;
import pexp.core.services.AccountService;
import pexp.core.services.exceptions.AccountExistsException;
import pexp.rest.resources.AccountResource;
import pexp.rest.resources.asm.AccountResourceAsm;

import javax.validation.Valid;
import java.util.List;


@Controller
public class AccountController {
    private AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

        @RequestMapping( value="/rest/accounts/{accountId}",
                method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<AccountResource> getAccount(
            @PathVariable Long accountId
    ) {
        Account account = accountService.findAccount(accountId);
        if(account != null)
        {
            AccountResource res = new AccountResourceAsm().toResource(account);
            return new ResponseEntity<>(res, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value="/signup", method = RequestMethod.GET)
    public String signup( ModelMap model) {
        model.addAttribute("account", new Account());
        return "signup";
    }

    @RequestMapping(value="/signup", method = RequestMethod.POST)
    public String signup(@Valid @ModelAttribute("account") AccountResource account, BindingResult result, RedirectAttributes redirect) {
        if (result.hasErrors())
        {
            return "signup";
        }
        try {
            accountService.createAccount(account.toAccount());
        } catch(AccountExistsException exception) {
            result.rejectValue("email", "error.user", "An account already exists for this email.");
            return "signup";
        }
        redirect.addFlashAttribute("globalMessage", "Successfully signed up");

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        UserDetails userDetails = new User(account.getEmail(), account.getPassword(), authorities);
        Authentication auth = new UsernamePasswordAuthenticationToken(userDetails, account.getPassword(), authorities);
        SecurityContextHolder.getContext().setAuthentication(auth);
        return "redirect:/";
    }
}
