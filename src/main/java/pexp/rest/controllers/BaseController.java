package pexp.rest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.context.ServletContextAware;
import pexp.core.models.entities.Account;
import pexp.core.services.AccountService;

import javax.servlet.ServletContext;

public class BaseController implements ServletContextAware {

    protected ServletContext servletContext;
    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    @Autowired
    private AccountService accountService;

    protected Account getLoggedInUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            UserDetails details = (UserDetails) principal;
            return accountService.findByEmail(details.getUsername());
        }
        return null;
    }
}
