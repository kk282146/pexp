package pexp.rest.controllers;

import pexp.rest.exceptions.NotFoundException;

public class RestPreconditions {
    public static <T> T checkFound(final T resource) {
        if (resource == null) {
            throw new NotFoundException();
        }
        return resource;
    }
}