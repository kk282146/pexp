package pexp.rest.resources;

import pexp.core.models.entities.Replies;
import org.springframework.hateoas.ResourceSupport;

import java.util.Date;
import java.sql.Timestamp;

public class RepliesResource extends ResourceSupport {
    private Long repliesId;
    private Long experimentId;
    private String answers;
    private Timestamp timeStamp;

    public Long getRepliesId() { return repliesId; }
    public void setRepliesId(Long id) { this.repliesId = id; }
    public Long getExperimentId() { return experimentId; }
    public void setExperimentId(Long experimentId) { this.experimentId = experimentId; }
    public String getAnswers() { return answers; }
    public void setAnswers(String answers) { this.answers = answers;}
    public Timestamp getTimeStamp() {return timeStamp;}
    public void setTimeStamp(Timestamp timeStamp) {this.timeStamp = timeStamp;}

    public Replies toReplies() {
        Replies obj = new Replies(answers);
        obj.setId(getRepliesId());
        Date date = new Date();
        obj.setTimeStamp(new Timestamp(date.getTime()));
        return obj;
    }
}