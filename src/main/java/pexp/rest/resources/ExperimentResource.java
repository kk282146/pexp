package pexp.rest.resources;

import org.springframework.hateoas.ResourceSupport;
import pexp.core.models.entities.Experiment;

public class ExperimentResource extends ResourceSupport {
    private Long expId;
    private String title;
    private String content;
    private String shareKey;

    public Long getExpId() {
        return expId;
    }
    public void setExpId(Long id) {
        this.expId = id;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public String getShareKey() {
        return shareKey;
    }
    public void setShareKey(String shareKey) {
        this.shareKey = shareKey;
    }

    public Experiment toExperiment() {
        Experiment obj = new Experiment(getTitle(), getContent());
        obj.setShareKey(getShareKey());
        obj.setId(getExpId());
        return obj;
    }
}