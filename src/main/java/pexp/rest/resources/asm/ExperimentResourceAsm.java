package pexp.rest.resources.asm;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import pexp.core.models.entities.Experiment;
import pexp.rest.controllers.AccountController;
import pexp.rest.controllers.ExperimentController;
import pexp.rest.resources.ExperimentResource;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

public class ExperimentResourceAsm extends ResourceAssemblerSupport<Experiment, ExperimentResource> {
    public ExperimentResourceAsm() {
        super(Experiment.class, ExperimentResource.class);
    }

    @Override
    public ExperimentResource toResource(Experiment experiment) {
        ExperimentResource res = new ExperimentResource();
        res.setTitle(experiment.getTitle());
        res.setContent(experiment.getContentAsStirng());
        res.setExpId(experiment.getId());
        res.setShareKey(experiment.getShareKey());
        Link self = linkTo(methodOn(ExperimentController.class).getExperiment(experiment.getShareKey())).withSelfRel();
        res.add(self);
        if(experiment.getAuthor() != null)
            res.add(linkTo(AccountController.class).slash(experiment.getAuthor().getId()).withRel("author"));
        return res;
    }
}
