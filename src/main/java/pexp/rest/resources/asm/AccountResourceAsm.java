package pexp.rest.resources.asm;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import pexp.core.models.entities.Account;
import pexp.rest.controllers.AccountController;
import pexp.rest.resources.AccountResource;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

//import pexp.rest.controllers.AccountController;

public class AccountResourceAsm extends ResourceAssemblerSupport<Account, AccountResource> {
    public AccountResourceAsm() {
        super(Account.class, AccountResource.class);
    }

    @Override
    public AccountResource toResource(Account account) {
        AccountResource res = new AccountResource();
        res.setFirstName(account.getFirstName());
        res.setLastName(account.getLastName());
        res.setEmail(account.getEmail());
        res.setPassword(account.getPassword());
        res.setRid(account.getId());
        res.add(linkTo(methodOn(AccountController.class).getAccount(account.getId())).withSelfRel());
        //res.add(linkTo(methodOn(AccountController.class).findAllBlogs(account.getId())).withRel("blogs"));
        return res;
    }
}
