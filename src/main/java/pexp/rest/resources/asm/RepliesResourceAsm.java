package pexp.rest.resources.asm;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import pexp.core.models.entities.Replies;
import pexp.rest.resources.RepliesResource;


public class RepliesResourceAsm extends ResourceAssemblerSupport<Replies, RepliesResource> {
    public RepliesResourceAsm() { super(Replies.class, RepliesResource.class); }

    @Override
    public RepliesResource toResource(Replies replies) {
        RepliesResource res = new RepliesResource();
        res.setAnswers(replies.getAnswers());
        res.setRepliesId(replies.getId());
        res.setTimeStamp(replies.getTimeStamp());

        return res;
    }
}