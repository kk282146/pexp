package pexp.rest.resources;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.ResourceSupport;
import pexp.core.models.entities.Account;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

public class AccountResource extends ResourceSupport {
    private Long rid;

    @Size(max = 130)
    private String firstName;

    @Size(max = 130)
    private String lastName;

    @Email @NotEmpty
    @Size(max = 130)
    private String email;

    @NotEmpty
    @Size(min=8, max = 70)
    private String password;

    public Long getRid() {
        return rid;
    }

    public void setRid(Long id) {
        this.rid = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String value) {
        this.firstName = value;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }

    public Account toAccount() {
        Account account = new Account();
        account.setFirstName(getFirstName());
        account.setLastName(getLastName());
        account.setEmail(getEmail());
        account.setPassword(getPassword());
        account.setId(getRid());
        return account;
    }
}
