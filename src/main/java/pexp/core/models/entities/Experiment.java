package pexp.core.models.entities;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import pexp.core.util.PGJsonObject;

import javax.persistence.*;

@Entity
@Table(name = "experiments")
@TypeDefs({@TypeDef( name= "JsonObject", typeClass = PGJsonObject.class)})
public class Experiment {
    
    public static int SHARE_KEY_LEN = 6;

    @Id
    @SequenceGenerator(name = "exp_seq", sequenceName = "experiments_id_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "exp_seq")
    private Long id;
    private String title;
    @Type(type = "JsonObject")
    private PGJsonObject content;
    
    @Column(name="share_key")
    private String shareKey;

    @OneToOne()
    private Account author;

    public Experiment() {
    }

    public Experiment(String title, String content) {
        this.title = title;
        setContent(content);
    }

    @Override
    public String toString() {
        return String.format(
                "Experiment[id=%d, title='%s']",
                id, title);
    }

    public Account getAuthor() {
        return author;
    }

    public void setAuthor(Account author) {
        this.author = author;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContentAsStirng() {
        return content != null ?
                content.getJsonString() : null;
    }
    @Type(type = "JsonObject")
    public PGJsonObject getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = new PGJsonObject(content);
    }

    public void setContent(PGJsonObject content) {
        this.content = content;
    }

    public String getShareKey() {
        return shareKey;
    }

    public void setShareKey(String shareKey) {
        this.shareKey = shareKey;
    }
}
