package pexp.core.models.entities;

import javax.persistence.*;

@Entity
@Table(name="accounts")
public class Account {
    @Id
    @SequenceGenerator(name="acc_seq", sequenceName="accounts_id_seq")
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="acc_seq")
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String password;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String value) {
        this.firstName = value;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

