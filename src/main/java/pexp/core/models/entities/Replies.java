package pexp.core.models.entities;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name="replies")
public class Replies {

    @Id
    @SequenceGenerator(name="repl_seq", sequenceName="replies_id_seq")
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="repl_seq")
    private Long id;
    private String answers;
    private Timestamp timeStamp;

    @OneToOne
    private Experiment experiment;
    
    public Replies() {}

    public Replies(String answers) {
        this.answers = answers;
    }

    @Override
    public String toString() {
        return String.format(
                "Replies[id=%d, %s, timeStamp= %s]",
                id, experiment, timeStamp);
    }

    public Account getAuthor() {
        return experiment.getAuthor();
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getAnswers() {
        return answers;
    }
    public void setAnswers(String answers) { this.answers = answers; }
    public Experiment getExperiment() {
        return experiment;
    }
    public void setExperiment(Experiment form) {
        this.experiment = form;
    }
    public Timestamp getTimeStamp() {return timeStamp;}
    public void setTimeStamp(Timestamp timeStamp) {this.timeStamp = timeStamp;}
}