package pexp.core.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import pexp.core.models.entities.Account;
import pexp.core.services.AccountService;

@Component
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private AccountService service;
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Account account = service.findByEmail(email);
        if(account == null) {
            throw new UsernameNotFoundException("no user found with email " + email);
        }
        return new AccountUserDetails(account);
    }
}
