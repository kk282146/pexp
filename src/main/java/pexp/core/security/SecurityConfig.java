package pexp.core.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebMvcSecurity
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled=true)
public class SecurityConfig extends WebSecurityConfigurerAdapter 
{
    /*@Order(1)
    @Configuration
    static class CreatorSecurityConfig extends WebSecurityConfigurerAdapter
    {
        @Autowired
        private AuthFailure authFailure;
        
        @Autowired
        private AuthSuccess authSuccess;
        
        @Autowired
        private EntryPointUnauthorizedHandler unauthorizedHandler;
        
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.antMatcher("/creator/**")
                .csrf().disable()
                .exceptionHandling()
                .authenticationEntryPoint(unauthorizedHandler)
                .and()
                .formLogin()
                .successHandler(authSuccess)
                .failureHandler(authFailure)
                .and()
                .authorizeRequests().antMatchers("/creator/index.html").authenticated()
                .anyRequest().permitAll();
        }        
    }*/

    @Autowired
    private UserDetailsServiceImpl userDetailService;

    @Autowired
    public void configAuthBuilder(AuthenticationManagerBuilder builder) throws Exception {
        builder.userDetailsService(userDetailService).passwordEncoder(passwordEncoder());

    }

    @Bean
    public PasswordEncoder passwordEncoder(){
           return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
            .authorizeRequests()
            .antMatchers("/", "/tester/**", "/creator/**","/signup", "/resources/**",
                    "/css/**", "/js/**", "/img/**", "/experiments/**").permitAll()
                .antMatchers("/**").authenticated()
            .and().formLogin().loginPage("/login").defaultSuccessUrl("/experiments", false).permitAll();
            //.and().logout().permitAll();
    }
}
