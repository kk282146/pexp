package pexp.core.repositories;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import pexp.core.models.entities.Replies;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RepliesRepo extends CrudRepository<Replies, Long>{
    List<Replies> findByExperimentId(long experimentId);
    @Modifying
    @Transactional
    @Query("delete from Replies r where r.experiment.id = ?1")
    void deleteByExperimentId(long experimentId);
}