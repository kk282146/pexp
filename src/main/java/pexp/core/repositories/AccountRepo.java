package pexp.core.repositories;

import org.springframework.data.repository.CrudRepository;
import pexp.core.models.entities.Account;

public interface AccountRepo extends CrudRepository<Account, Long> {
    public Account findByEmail(String email);
}
