package pexp.core.repositories;

import org.springframework.data.jpa.repository.Query;
import pexp.core.models.entities.Experiment;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ExperimentRepo extends CrudRepository<Experiment, Long>{
    Experiment findByTitleAndAuthorId(String title, long authorId);
    List<Experiment> findByAuthorIdOrderByIdAsc(long authorId);
    Experiment findByShareKey(String shareKey);
    
    @Query("SELECT CASE WHEN COUNT(e) > 0 THEN 'true' ELSE 'false' END FROM Experiment e WHERE e.shareKey = ?1")
    public Boolean existsByShareKey(String shareKey);
}
