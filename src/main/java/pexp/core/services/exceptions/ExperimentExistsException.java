package pexp.core.services.exceptions;


public class ExperimentExistsException extends RuntimeException {
    public ExperimentExistsException() {
    }

    public ExperimentExistsException(String message) {
        super(message);
    }

    public ExperimentExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public ExperimentExistsException(Throwable cause) {
        super(cause);
    }
}
