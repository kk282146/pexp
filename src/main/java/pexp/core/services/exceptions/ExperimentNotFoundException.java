package pexp.core.services.exceptions;

public class ExperimentNotFoundException extends RuntimeException {
    public ExperimentNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ExperimentNotFoundException(String message) {
        super(message);
    }

    public ExperimentNotFoundException() {
    }
}
