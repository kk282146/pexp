package pexp.core.services;

import pexp.core.models.entities.Replies;
import java.util.List;

public interface RepliesService {
    public List<Replies> findRepliesByExperiment(Long experimentId);
    public Replies saveReplies(Long experimentId, Replies replies);
}
