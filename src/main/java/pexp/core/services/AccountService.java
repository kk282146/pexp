package pexp.core.services;

import pexp.core.models.entities.Account;

public interface AccountService {
    public Account findAccount(Long id);
    public Account createAccount(Account data);
    public Iterable<Account> findAllAccounts();
    public Account findByEmail(String email);
}
