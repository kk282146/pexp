package pexp.core.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pexp.core.models.entities.Account;
import pexp.core.repositories.AccountRepo;
import pexp.core.services.AccountService;
import pexp.core.services.exceptions.AccountExistsException;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountRepo accountRepo;

    @Override
    public Account findAccount(Long id) {
        return accountRepo.findOne(id);
    }

    public static String encodePassword(String notEncodedPassw)
    {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(notEncodedPassw);
    }

    @Override
    public Account createAccount(Account data) {
        Account account = accountRepo.findByEmail(data.getEmail());
        if(account != null)
        {
            throw new AccountExistsException();
        }

        data.setPassword(encodePassword(data.getPassword()));
        return accountRepo.save(data);
    }

    @Override
    public Iterable<Account> findAllAccounts() {
        return  accountRepo.findAll();
    }

    @Override
    public Account findByEmail(String email) {
        return accountRepo.findByEmail(email);
    }
}
