package pexp.core.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pexp.core.models.entities.Experiment;
import pexp.core.models.entities.Replies;
import pexp.core.repositories.ExperimentRepo;
import pexp.core.repositories.RepliesRepo;
import pexp.core.services.RepliesService;
import pexp.core.services.exceptions.ExperimentNotFoundException;

import java.util.List;

@Service
@Transactional
public class RepliesServiceImpl implements RepliesService{

    @Autowired
    private ExperimentRepo experimentRepo;

    @Autowired
    private RepliesRepo repliesRepo;

    @Override
    public List<Replies> findRepliesByExperiment(Long experimentId) {
        Experiment experiment = experimentRepo.findOne(experimentId);
        if (experiment == null)
        {
            throw new ExperimentNotFoundException();
        }
        return repliesRepo.findByExperimentId(experimentId);
    }

    @Override
    public Replies saveReplies(Long experimentId, Replies replies) {
        Experiment experiment = experimentRepo.findOne(experimentId);

        if (experiment == null) {
            throw new ExperimentNotFoundException();
        }

        replies.setExperiment(experiment);

        return repliesRepo.save(replies);
    }
}
