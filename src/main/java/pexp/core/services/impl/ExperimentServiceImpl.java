package pexp.core.services.impl;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pexp.core.models.entities.Account;
import pexp.core.models.entities.Experiment;
import pexp.core.repositories.AccountRepo;
import pexp.core.repositories.ExperimentRepo;
import pexp.core.repositories.RepliesRepo;
import pexp.core.services.ExperimentService;
import pexp.core.services.exceptions.AccountDoesNotExistException;
import pexp.core.services.exceptions.ExperimentExistsException;
import pexp.core.services.exceptions.ExperimentNotFoundException;
import pexp.rest.exceptions.ForbiddenException;

import java.util.List;

@Service
@Transactional
public class ExperimentServiceImpl implements ExperimentService {

    @Autowired
    private ExperimentRepo experimentRepo;

    @Autowired
    private RepliesRepo repliesRepo;

    @Autowired
    private AccountRepo accountRepo;

    @Override
    public Experiment findById(Long id) {
        return experimentRepo.findOne(id);
    }

    @Override
    public Experiment findByShareKey(String shareKey) {
        return experimentRepo.findByShareKey(shareKey);
    }

    @Override
    public Experiment saveExperiment(Long authorId, Experiment experiment) {

        Experiment experiment2 = experimentRepo.findByTitleAndAuthorId(experiment.getTitle(), authorId);

        if (experiment2 != null) {
            throw new ExperimentExistsException();
        }

        Account author = accountRepo.findOne(authorId);
        if (author == null) {
            throw new AccountDoesNotExistException();
        }

        experiment.setAuthor(author);
        
        if (experiment.getShareKey() == null || experiment.getShareKey().isEmpty()) {
            String shareKey = generateUniqueExperimentKey();
            experiment.setShareKey(shareKey);
        }

        return experimentRepo.save(experiment);
    }

    public Experiment updateExperiment(Long currentUserId, Experiment experiment)
    {
        Experiment experiment2 = null;

        if (experiment != null && experiment.getShareKey() != null)
            experiment2 = experimentRepo.findByShareKey(experiment.getShareKey());

        if (experiment2 == null) {
            throw new ExperimentNotFoundException();
        }

        Account author = accountRepo.findOne(currentUserId);
        if (author == null) {
            //unbelievable case when current user was deleted just at the moment
            throw new AccountDoesNotExistException();
        }

        //check if the currently logged user have permissions to edit the experiment
        if (!experiment2.getAuthor().getId().equals(currentUserId))
            throw new ForbiddenException();

        experiment2.setContent(experiment.getContent());
        experiment2.setTitle(experiment.getTitle());

        return experimentRepo.save(experiment2);
    }

    @Override
    public String generateUniqueExperimentKey() {
        Boolean generatedUniqueKey = false;
        String shareKey = "";
        while (!generatedUniqueKey) {
            shareKey = RandomStringUtils.random(Experiment.SHARE_KEY_LEN, true, true).toLowerCase();
            generatedUniqueKey = !experimentRepo.existsByShareKey(shareKey);
        }
        return shareKey;
    }

    @Override
    public List<Experiment> findExperimentsByAuthor(Long accountId) {
        Account account = accountRepo.findOne(accountId);
        if(account == null)
        {
            throw new AccountDoesNotExistException();
        }
        return experimentRepo.findByAuthorIdOrderByIdAsc(accountId);
    }

    public void deleteExperiment(Long currentUserId, String experimentShareKey)
    {
        Experiment experiment = experimentRepo.findByShareKey(experimentShareKey);

        if (experiment == null) {
            throw new ExperimentNotFoundException();
        }

        Account author = accountRepo.findOne(currentUserId);
        if (author == null) {
            throw new AccountDoesNotExistException();
        }

        if (!experiment.getAuthor().getId().equals(currentUserId))
            throw new ForbiddenException();

        repliesRepo.deleteByExperimentId(experiment.getId());
        experimentRepo.delete(experiment.getId());
    }
}
