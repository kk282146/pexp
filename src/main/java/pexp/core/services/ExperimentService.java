package pexp.core.services;

import pexp.core.models.entities.Experiment;

import java.util.List;

public interface ExperimentService {
    public Experiment findById(Long id);
    public List<Experiment> findExperimentsByAuthor(Long accountId);
    public Experiment findByShareKey(String shareKey);
    public Experiment saveExperiment(Long authorId, Experiment experiment);
    public Experiment updateExperiment(Long currentUserId, Experiment experiment);
    public String generateUniqueExperimentKey();
    public void deleteExperiment(Long currentUserId, String experimentShareKey);
}
